use crate::SharedStr;

use super::SharedBytes;
use quickcheck::Arbitrary;
use std::{convert::identity, ops::Bound, sync::Arc};

type Bounds = (Bound<usize>, Bound<usize>);

#[test]
fn qc_try_utf_8_into_shared_string() {
    quickcheck::quickcheck(
        test_shared_string_from_utf_8 as fn(SharedBytesFromVecFn, Vec<u8>, Bounds),
    )
}

fn test_shared_string_from_utf_8(new: SharedBytesFromVecFn, bytes: Vec<u8>, bounds: Bounds) {
    let shared_bytes = new
        .call(bytes)
        .try_slice_into(bounds)
        .unwrap_or_else(identity);
    match SharedStr::from_utf8(shared_bytes.clone()) {
        Ok(actual_string) => assert_eq!(actual_string.as_bytes(), shared_bytes.as_slice()),
        Err(_) => {
            let _error = std::str::from_utf8(shared_bytes.as_slice()).unwrap_err();
        }
    }
}

#[test]
fn qc_try_slice_to_subset() {
    quickcheck::quickcheck(test_try_slice_to_subset as fn(SharedBytesFromVecFn, Vec<u8>, Bounds))
}

fn test_try_slice_to_subset(new: SharedBytesFromVecFn, bytes: Vec<u8>, bounds: Bounds) {
    let shared_bytes = new.call(bytes);
    let subset = match shared_bytes.get(bounds) {
        Some(x) => x,
        None => return,
    };
    let actual = shared_bytes
        .clone()
        .try_slice_into(shared_bytes.range_of_subset(subset))
        .unwrap();
    assert_eq!(actual, subset);
}

#[test]
fn try_slice_range() {
    for &new in SHARED_BYTES_FROM_STATIC_FNS {
        let actual_1 = new(&[0, 1, 2, 3, 4, 5, 6]).try_slice_into(2..6).unwrap();
        assert_eq!(actual_1, &[2, 3, 4, 5]);
        let actual_2 = actual_1.try_slice_into(1..3).unwrap();
        assert_eq!(actual_2, &[3, 4]);
    }
}

#[test]
fn try_slice_range_to() {
    for &new in SHARED_BYTES_FROM_STATIC_FNS {
        let actual_1 = new(&[0, 1, 2, 3, 4, 5, 6]).try_slice_into(..6).unwrap();
        assert_eq!(actual_1, &[0, 1, 2, 3, 4, 5]);
        let actual_2 = actual_1.try_slice_into(..3).unwrap();
        assert_eq!(actual_2, &[0, 1, 2]);
    }
}

#[test]
fn try_slice_range_from() {
    for &new in SHARED_BYTES_FROM_STATIC_FNS {
        let actual_1 = new(&[0, 1, 2, 3, 4, 5, 6]).try_slice_into(3..).unwrap();
        assert_eq!(actual_1, &[3, 4, 5, 6]);
        let actual_2 = actual_1.try_slice_into(1..).unwrap();
        assert_eq!(actual_2, &[4, 5, 6]);
    }
}

#[test]
fn try_slice_inside_utf_8_char() {
    let string = "€".to_owned();
    assert_eq!(string.as_bytes(), [226, 130, 172].as_slice());
    let shared_bytes = SharedBytes::from(Arc::new(string));
    let actual = shared_bytes.slice_into(1..2);
    assert_eq!(actual, &[130]);
}

#[test]
fn qc_shared_bytes_slices_like_std() {
    quickcheck::quickcheck(
        test_shared_bytes_slices_like_std as fn(Vec<u8>, SharedBytesFromVecFn, Vec<Bounds>),
    )
}

#[test]
fn shared_bytes_slices_like_std_max_upper_bound() {
    test_shared_bytes_slices_like_std(
        vec![0],
        SharedBytesFromVecFn(SharedBytes::from_vec),
        vec![
            (Bound::Excluded(0), Bound::Unbounded),
            (Bound::Unbounded, Bound::Included(usize::MAX)),
        ],
    )
}

fn test_shared_bytes_slices_like_std(
    bytes: Vec<u8>,
    new: SharedBytesFromVecFn,
    bounds: Vec<Bounds>,
) {
    let mut shared_bytes = new.call(bytes.clone());
    let mut std_slice = bytes.as_slice();
    for bound in bounds {
        let actual_result = shared_bytes.try_slice_into(bound);
        let expected_result = std_slice.get(bound).ok_or(std_slice);
        match actual_result {
            Err(actual) => {
                let expected = expected_result.unwrap_err();
                assert_eq!(actual, expected);
                return;
            }
            Ok(actual) => {
                let expected = expected_result.unwrap();
                assert_eq!(actual, expected);
                shared_bytes = actual;
                std_slice = expected;
            }
        }
    }
}

#[test]
fn qc_try_split_off() {
    quickcheck::quickcheck(test_try_split_off as fn(SharedBytesFromVecFn, Vec<u8>, Bounds, usize))
}

#[test]
fn try_split_off_max_index() {
    test_try_split_off(
        SharedBytesFromVecFn(SharedBytes::from_vec),
        vec![0],
        (Bound::Excluded(0), Bound::Unbounded),
        usize::MAX,
    )
}

fn test_try_split_off(new: SharedBytesFromVecFn, bytes: Vec<u8>, bounds: Bounds, index: usize) {
    let mut expected_bytes = match bytes.get(bounds) {
        Some(b) => b.to_owned(),
        None => bytes.clone(),
    };
    let mut actual_bytes = new
        .call(bytes)
        .try_slice_into(bounds)
        .unwrap_or_else(identity);
    match actual_bytes.try_split_off(index) {
        None => assert_eq!(expected_bytes.get(index), None),
        Some(actual_split) => {
            let expected_split = expected_bytes.split_off(index);
            assert_eq!(actual_split, expected_split);
            assert_eq!(actual_bytes, expected_bytes);
        }
    };
}

const SHARED_BYTES_FROM_STATIC_FNS: &[fn(&'static [u8]) -> SharedBytes] = &[
    SharedBytes::from_static,
    |b| SharedBytes::from_vec(b.to_owned()),
    |b| SharedBytes::from(Arc::new(b.to_owned())),
    |b| match std::str::from_utf8(b) {
        Ok(s) => SharedBytes::from(Arc::new(s.to_owned())),
        Err(_) => SharedBytes::from(Arc::new(b.to_owned())),
    },
];

const SHARED_BYTES_FROM_VEC_FNS: &[fn(Vec<u8>) -> SharedBytes] = &[
    shared_bytes_static_from_vec_leak,
    SharedBytes::from_vec,
    shared_bytes_arc_vec_from_vec,
    shared_bytes_arc_string_from_vec,
];

fn shared_bytes_static_from_vec_leak(b: Vec<u8>) -> SharedBytes {
    SharedBytes::from_static(b.leak())
}

fn shared_bytes_arc_vec_from_vec(b: Vec<u8>) -> SharedBytes {
    SharedBytes::from_arc_vec(Arc::new(b))
}

fn shared_bytes_arc_string_from_vec(b: Vec<u8>) -> SharedBytes {
    match std::str::from_utf8(&b) {
        Ok(s) => SharedBytes::from_arc_string(Arc::new(s.to_owned())),
        Err(_) => SharedBytes::from_arc_vec(Arc::new(b)),
    }
}

#[derive(Debug, Clone, Copy)]
struct SharedBytesFromVecFn(fn(Vec<u8>) -> SharedBytes);

impl SharedBytesFromVecFn {
    fn call(self, b: Vec<u8>) -> SharedBytes {
        (self.0)(b)
    }
}

impl Arbitrary for SharedBytesFromVecFn {
    fn arbitrary(g: &mut quickcheck::Gen) -> Self {
        Self(*g.choose(SHARED_BYTES_FROM_VEC_FNS).unwrap())
    }
}
