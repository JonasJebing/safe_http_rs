use super::SharedBytes;
use std::{
    iter::{self, FusedIterator},
    slice,
};

#[derive(Debug, Clone)]
pub struct Iter<'a>(iter::Copied<slice::Iter<'a, u8>>);

impl<'a> Iter<'a> {
    pub(super) fn new(i: &'a [u8]) -> Self {
        Self(i.iter().copied())
    }
}

impl Iterator for Iter<'_> {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl DoubleEndedIterator for Iter<'_> {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.0.next_back()
    }
}

impl ExactSizeIterator for Iter<'_> {
    fn len(&self) -> usize {
        self.0.len()
    }
}

impl FusedIterator for Iter<'_> {}

#[derive(Debug, Clone)]
pub struct IntoIter(pub(super) SharedBytes);

impl Iterator for IntoIter {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        let next = *self.0.first()?;
        self.0.try_slice_mut(1..).unwrap();
        Some(next)
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let len = self.0.len();
        (len, Some(len))
    }
}

impl DoubleEndedIterator for IntoIter {
    fn next_back(&mut self) -> Option<Self::Item> {
        let next = *self.0.last()?;
        self.0.try_slice_mut(..self.0.len() - 1).unwrap();
        Some(next)
    }
}

impl ExactSizeIterator for IntoIter {
    fn len(&self) -> usize {
        self.0.len()
    }
}

impl FusedIterator for IntoIter {}

#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::Arc;

    #[test]
    fn qc_into_iter() {
        quickcheck::quickcheck(test_into_iter as fn(Vec<u8>))
    }

    fn test_into_iter(b: Vec<u8>) {
        let bytes = Arc::new(b);
        let shared_bytes = SharedBytes::from(bytes.clone());
        let actual = Vec::from_iter(shared_bytes.into_iter());
        assert_eq!(actual, *bytes);
    }

    #[test]
    fn qc_into_iter_reverse() {
        quickcheck::quickcheck(test_into_iter_reverse as fn(Vec<u8>))
    }

    fn test_into_iter_reverse(mut bytes: Vec<u8>) {
        let shared_bytes = SharedBytes::from(bytes.clone());
        let actual = Vec::from_iter(shared_bytes.into_iter().rev());
        bytes.reverse();
        assert_eq!(actual, bytes);
    }
}
