use std::sync::Arc;

/// A marker trait for types that don't have interior mutability.
///
/// # Safety
///
/// Safe to implement for types that
/// 1. don't allow interior mutability
/// 2. don't drop their data as long as an instance of the type is alive
///
/// For example it is safe to implement for [`Arc<T>`] where `T` implements [`NoInteriorMutability`]
/// because [`Arc`] on its own does not allow mutable access to its inner data.
/// Also [`Arc`] only provides copies of its strong and weak reference count.
///
/// As a counter-example it is not safe to implement for any [`Weak<T>`](std::sync::Weak)
/// because the data may be dropped when the strong reference count is decremented to zero.
pub(crate) unsafe trait NoInteriorMutability {}

unsafe impl NoInteriorMutability for [u8] {}

unsafe impl NoInteriorMutability for Vec<u8> {}

unsafe impl NoInteriorMutability for str {}

unsafe impl NoInteriorMutability for String {}

unsafe impl<T: NoInteriorMutability> NoInteriorMutability for Arc<T> {}
