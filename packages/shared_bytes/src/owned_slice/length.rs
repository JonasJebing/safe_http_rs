pub(crate) trait Length {
    fn length(&self) -> usize;
}

impl<T> Length for [T] {
    fn length(&self) -> usize {
        self.len()
    }
}

impl Length for str {
    fn length(&self) -> usize {
        self.len()
    }
}
