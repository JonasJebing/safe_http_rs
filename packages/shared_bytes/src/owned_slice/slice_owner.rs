use std::{ops::Deref, sync::Arc};

pub(crate) trait SliceOwner {
    type Slice: ?Sized;

    fn as_slice(&self) -> &Self::Slice;
}

impl<Owner> SliceOwner for Arc<Owner>
where
    Owner: SliceOwner,
{
    type Slice = Owner::Slice;

    fn as_slice(&self) -> &Self::Slice {
        self.deref().as_slice()
    }
}

impl<T> SliceOwner for Vec<T> {
    type Slice = [T];
    fn as_slice(&self) -> &[T] {
        self
    }
}

impl SliceOwner for String {
    type Slice = str;
    fn as_slice(&self) -> &str {
        self
    }
}

impl<T> SliceOwner for [T] {
    type Slice = [T];
    fn as_slice(&self) -> &[T] {
        self
    }
}

impl SliceOwner for str {
    type Slice = str;
    fn as_slice(&self) -> &str {
        self
    }
}
