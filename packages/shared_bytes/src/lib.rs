mod errors;
mod owned_slice;
mod range_of_subset;
mod shared_bytes;
mod shared_str;
mod util;

pub use self::{
    errors::IndexOutOfBounds,
    range_of_subset::RangeOfSubset,
    shared_bytes::{IntoIter, Iter, SharedBytes},
    shared_str::SharedStr,
};

const UNSAFE_OPTIMISATIONS_DEFAULT: bool = true;

#[allow(clippy::if_same_then_else)] // seems to be a false positive by clippy
const UNSAFE_OPTIMISATIONS: bool = if cfg!(test) {
    // if --all-features { true } else { false }
    cfg!(feature = "unstable_feature_unsafe_optimizations")
} else if cfg!(feature = "unstable_feature_no_unsafe_optimizations") {
    false
} else if cfg!(feature = "unstable_feature_unsafe_optimizations") {
    true
} else {
    UNSAFE_OPTIMISATIONS_DEFAULT
};
