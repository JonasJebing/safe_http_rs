use std::ops::{Bound, Range, RangeBounds};

pub fn from_slice_bounds<R: RangeBounds<usize>>(
    range: R,
    len: impl FnOnce() -> usize,
) -> Range<usize> {
    let start = match range.start_bound().cloned() {
        Bound::Included(i) => i,
        Bound::Excluded(i) => i.saturating_add(1),
        Bound::Unbounded => 0,
    };
    let end = match range.end_bound().cloned() {
        Bound::Included(i) => i.saturating_add(1),
        Bound::Excluded(i) => i,
        Bound::Unbounded => len(),
    };
    start..end
}
