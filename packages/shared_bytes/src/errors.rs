use std::{error::Error, fmt};

#[derive(Debug, Clone)] // Not Copy because we might add a String to this struct later on.
pub struct IndexOutOfBounds(());

impl IndexOutOfBounds {
    pub(crate) fn new() -> Self {
        Self(())
    }
}

impl fmt::Display for IndexOutOfBounds {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("index out of bounds")
    }
}

impl Error for IndexOutOfBounds {}
