#![forbid(unsafe_code)]

mod uri;
mod uri_ref;

pub use uri::UriAdapter;
pub use uri_ref::UriRefAdapter;
