use safe_uri::UriRef;
#[cfg(feature = "serde")]
use serde::de::Error;
use std::fmt;

/// An [`UriRef`] adapter, which implements extra traits and functionality.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct UriRefAdapter(pub UriRef);

impl fmt::Display for UriRefAdapter {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", &self.0)
    }
}

#[cfg(feature = "serde")]
impl serde::Serialize for UriRefAdapter {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.0.to_string())
    }
}

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for UriRefAdapter {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        let this = UriRef::parse(s.into())
            .map_err(|e| D::Error::custom(format_args!("{:#}", anyhow::Error::new(e))))?;
        Ok(Self(this))
    }
}
