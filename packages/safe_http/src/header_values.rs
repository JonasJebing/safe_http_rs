mod iter;

pub use iter::{IntoIter, Iter, IterMut};

use crate::HeaderValue;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct HeaderValues {
    first: HeaderValue,
    remaining: Vec<HeaderValue>,
}

impl HeaderValues {
    #[inline]
    pub fn new(first: HeaderValue) -> Self {
        Self {
            first,
            remaining: Vec::new(),
        }
    }

    pub fn has_only_one(&self) -> bool {
        self.remaining.is_empty()
    }

    pub fn push(&mut self, v: HeaderValue) {
        self.remaining.push(v);
    }

    pub fn first(&self) -> &HeaderValue {
        &self.first
    }

    pub fn first_mut(&mut self) -> &mut HeaderValue {
        &mut self.first
    }

    pub fn into_first(self) -> HeaderValue {
        self.first
    }

    pub fn iter(&self) -> Iter<'_> {
        Iter {
            first: Some(&self.first),
            remaining: self.remaining.iter(),
        }
    }

    pub fn iter_mut(&mut self) -> IterMut<'_> {
        IterMut {
            first: Some(&mut self.first),
            remaining: self.remaining.iter_mut(),
        }
    }
}

impl From<HeaderValue> for HeaderValues {
    #[inline]
    fn from(v: HeaderValue) -> Self {
        Self::new(v)
    }
}

impl FromIterator<HeaderValue> for Option<HeaderValues> {
    fn from_iter<T: IntoIterator<Item = HeaderValue>>(iter: T) -> Self {
        let mut iter = iter.into_iter();
        let first = iter.next()?;
        let remaining = iter.collect();
        Some(HeaderValues { first, remaining })
    }
}

impl Extend<HeaderValue> for HeaderValues {
    fn extend<T: IntoIterator<Item = HeaderValue>>(&mut self, iter: T) {
        self.remaining.extend(iter)
    }
}

impl IntoIterator for HeaderValues {
    type Item = HeaderValue;

    type IntoIter = IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter {
            first: Some(self.first),
            remaining: self.remaining.into_iter(),
        }
    }
}

impl<'r> IntoIterator for &'r HeaderValues {
    type Item = &'r HeaderValue;

    type IntoIter = Iter<'r>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}
