use safe_uri::Uri;

use crate::{method::Method, version::Version};

#[derive(Debug, Clone, PartialEq, Eq, Hash, Default)]
pub struct RequestLine {
    pub method: Method,
    pub uri: Uri,
    pub version: Version,
    #[doc(hidden)]
    pub __private: (),
}

impl RequestLine {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn from_uri(uri: Uri) -> Self {
        Self {
            method: Method::default(),
            uri,
            version: Version::latest(),
            __private: (),
        }
    }
}

impl From<Uri> for RequestLine {
    #[inline]
    fn from(uri: Uri) -> Self {
        Self::from_uri(uri)
    }
}
