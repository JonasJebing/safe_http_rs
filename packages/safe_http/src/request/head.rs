use super::RequestLine;
use crate::HeaderMap;

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct RequestHead {
    pub line: RequestLine,
    pub headers: HeaderMap,
    #[doc(hidden)]
    pub __private: (),
}

impl RequestHead {
    pub fn new() -> Self {
        Default::default()
    }
}
