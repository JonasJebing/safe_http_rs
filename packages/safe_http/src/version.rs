mod constants;

use std::fmt;

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Version(u8, u8);

impl Version {
    pub(crate) const fn latest() -> Self {
        Self::HTTP_2_0
    }
}

impl Default for Version {
    /// Currently the default is the latest accepted standard.
    ///
    /// # Compatibility
    ///
    /// The default value may change in future versions and is not considered a breaking change.
    fn default() -> Self {
        Self::HTTP_2_0
    }
}

impl fmt::Display for Version {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "HTTP/{}.{}", self.0, self.1)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn display() {
        assert_eq!(Version::HTTP_1_0.to_string(), "HTTP/1.0")
    }
}
