use crate::{status::StatusCode, version::Version};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Default)]
pub struct ResponseLine {
    pub version: Version,
    pub status_code: StatusCode,
    #[doc(hidden)]
    pub __private: (),
}

impl ResponseLine {
    pub fn new() -> Self {
        Self::default()
    }

    const fn from_status_code(status_code: StatusCode) -> Self {
        Self {
            version: Version::latest(),
            status_code,
            __private: (),
        }
    }
}

impl From<StatusCode> for ResponseLine {
    #[inline]
    fn from(status_code: StatusCode) -> Self {
        Self::from_status_code(status_code)
    }
}
