use super::ResponseLine;
use crate::HeaderMap;

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct ResponseHead {
    pub line: ResponseLine,
    pub headers: HeaderMap,
    #[doc(hidden)]
    pub __private: (),
}

impl ResponseHead {
    pub fn new() -> Self {
        Default::default()
    }
}
