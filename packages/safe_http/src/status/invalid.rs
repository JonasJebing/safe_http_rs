use std::{error::Error, fmt};

#[derive(Debug, Clone)]
pub struct InvalidStatusCode {
    pub(crate) code: u16,
}

impl fmt::Display for InvalidStatusCode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} is an invalid StatusCode. The valid range is 100-599",
            self.code
        )
    }
}

impl Error for InvalidStatusCode {}
