use crate::{HeaderName, HeaderValues};
use std::collections::hash_map;

#[derive(Debug)]
pub enum Entry<'map> {
    Occupied(OccupiedEntry<'map>),
    Vacant(VacantEntry<'map>),
}

impl<'map> Entry<'map> {
    pub fn and_modify<F>(self, f: F) -> Self
    where
        F: FnOnce(&mut HeaderValues),
    {
        match self {
            Entry::Occupied(mut x) => {
                f(x.get_mut());
                Entry::Occupied(x)
            }
            Entry::Vacant(x) => Entry::Vacant(x),
        }
    }

    pub fn key(&self) -> &HeaderName {
        match self {
            Entry::Occupied(x) => x.key(),
            Entry::Vacant(x) => x.key(),
        }
    }

    pub fn or_insert(self, default: HeaderValues) -> &'map mut HeaderValues {
        self.or_insert_with(|| default)
    }

    pub fn or_insert_with<F: FnOnce() -> HeaderValues>(self, default: F) -> &'map mut HeaderValues {
        self.or_insert_with_key(|_| default())
    }

    pub fn or_insert_with_key<F: FnOnce(&HeaderName) -> HeaderValues>(
        self,
        default: F,
    ) -> &'map mut HeaderValues {
        match self {
            Entry::Occupied(x) => x.into_mut(),
            Entry::Vacant(entry) => {
                let value = default(entry.key());
                entry.insert(value)
            }
        }
    }
}

type OccupiedInner<'map> = hash_map::OccupiedEntry<'map, HeaderName, HeaderValues>;

#[derive(Debug)]
pub struct OccupiedEntry<'map>(pub(super) OccupiedInner<'map>);

impl<'map> OccupiedEntry<'map> {
    pub fn key(&self) -> &HeaderName {
        self.0.key()
    }

    pub fn get(&self) -> &HeaderValues {
        self.0.get()
    }

    pub fn get_mut(&mut self) -> &mut HeaderValues {
        self.0.get_mut()
    }

    pub fn insert(&mut self, values: HeaderValues) -> HeaderValues {
        self.0.insert(values)
    }

    pub fn into_mut(self) -> &'map mut HeaderValues {
        self.0.into_mut()
    }

    pub fn remove(self) -> HeaderValues {
        self.0.remove()
    }

    pub fn remove_entry(self) -> (HeaderName, HeaderValues) {
        self.0.remove_entry()
    }
}

type VacantInner<'map> = hash_map::VacantEntry<'map, HeaderName, HeaderValues>;

#[derive(Debug)]
pub struct VacantEntry<'map>(pub(super) VacantInner<'map>);

impl<'map> VacantEntry<'map> {
    pub fn key(&self) -> &HeaderName {
        self.0.key()
    }

    pub fn into_key(self) -> HeaderName {
        self.0.into_key()
    }

    pub fn insert(self, values: HeaderValues) -> &'map mut HeaderValues {
        self.0.insert(values)
    }
}
