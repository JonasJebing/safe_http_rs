use crate::{HeaderName, HeaderValues};
use std::{collections::hash_map, iter::FusedIterator};

#[derive(Debug)]
pub struct Drain<'map>(pub(super) hash_map::Drain<'map, HeaderName, HeaderValues>);

impl<'map> Iterator for Drain<'map> {
    type Item = (HeaderName, HeaderValues);

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl ExactSizeIterator for Drain<'_> {}

impl FusedIterator for Drain<'_> {}

#[derive(Debug)]
pub struct IntoIter(pub(super) hash_map::IntoIter<HeaderName, HeaderValues>);

impl Iterator for IntoIter {
    type Item = (HeaderName, HeaderValues);

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl ExactSizeIterator for IntoIter {}

impl FusedIterator for IntoIter {}

#[derive(Debug)]
pub struct IntoKeys(pub(super) hash_map::IntoKeys<HeaderName, HeaderValues>);

impl Iterator for IntoKeys {
    type Item = HeaderName;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl ExactSizeIterator for IntoKeys {}

impl FusedIterator for IntoKeys {}

#[derive(Debug)]
pub struct IntoValues(pub(super) hash_map::IntoValues<HeaderName, HeaderValues>);

impl Iterator for IntoValues {
    type Item = HeaderValues;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl ExactSizeIterator for IntoValues {}

impl FusedIterator for IntoValues {}

#[derive(Debug, Clone)]
pub struct Iter<'map>(pub(super) hash_map::Iter<'map, HeaderName, HeaderValues>);

impl<'map> Iterator for Iter<'map> {
    type Item = (&'map HeaderName, &'map HeaderValues);

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl ExactSizeIterator for Iter<'_> {}

impl FusedIterator for Iter<'_> {}

#[derive(Debug)]
pub struct IterMut<'map>(pub(super) hash_map::IterMut<'map, HeaderName, HeaderValues>);

impl<'map> Iterator for IterMut<'map> {
    type Item = (&'map HeaderName, &'map mut HeaderValues);

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl ExactSizeIterator for IterMut<'_> {}

impl FusedIterator for IterMut<'_> {}

#[derive(Debug, Clone)]
pub struct Keys<'map>(pub(super) hash_map::Keys<'map, HeaderName, HeaderValues>);

impl<'map> Iterator for Keys<'map> {
    type Item = &'map HeaderName;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl ExactSizeIterator for Keys<'_> {}

impl FusedIterator for Keys<'_> {}

#[derive(Debug, Clone)]
pub struct Values<'map>(pub(super) hash_map::Values<'map, HeaderName, HeaderValues>);

impl<'map> Iterator for Values<'map> {
    type Item = &'map HeaderValues;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl ExactSizeIterator for Values<'_> {}

impl FusedIterator for Values<'_> {}

#[derive(Debug)]
pub struct ValuesMut<'map>(pub(super) hash_map::ValuesMut<'map, HeaderName, HeaderValues>);

impl<'map> Iterator for ValuesMut<'map> {
    type Item = &'map mut HeaderValues;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl ExactSizeIterator for ValuesMut<'_> {}

impl FusedIterator for ValuesMut<'_> {}
