use crate::HeaderValue;
use std::{iter::FusedIterator, slice, vec};

#[derive(Debug, Clone)]
pub struct IntoIter {
    pub(super) first: Option<HeaderValue>,
    pub(super) remaining: vec::IntoIter<HeaderValue>,
}

impl Iterator for IntoIter {
    type Item = HeaderValue;

    fn next(&mut self) -> Option<HeaderValue> {
        self.first.take().or_else(|| self.remaining.next())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        if self.first.is_some() {
            let length = self.remaining.len() + 1;
            (length, Some(length))
        } else {
            self.remaining.size_hint()
        }
    }
}

impl DoubleEndedIterator for IntoIter {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.remaining.next_back().or_else(|| self.first.take())
    }
}

impl ExactSizeIterator for IntoIter {}

impl FusedIterator for IntoIter {}

#[derive(Debug, Clone)]
pub struct Iter<'r> {
    pub(super) first: Option<&'r HeaderValue>,
    pub(super) remaining: slice::Iter<'r, HeaderValue>,
}

impl<'r> Iterator for Iter<'r> {
    type Item = &'r HeaderValue;

    fn next(&mut self) -> Option<Self::Item> {
        self.first.take().or_else(|| self.remaining.next())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        if self.first.is_some() {
            let length = self.remaining.len() + 1;
            (length, Some(length))
        } else {
            self.remaining.size_hint()
        }
    }
}

impl DoubleEndedIterator for Iter<'_> {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.remaining.next_back().or_else(|| self.first.take())
    }
}

impl ExactSizeIterator for Iter<'_> {}

impl FusedIterator for Iter<'_> {}

#[derive(Debug)]
pub struct IterMut<'r> {
    pub(super) first: Option<&'r mut HeaderValue>,
    pub(super) remaining: slice::IterMut<'r, HeaderValue>,
}

impl<'r> Iterator for IterMut<'r> {
    type Item = &'r mut HeaderValue;

    fn next(&mut self) -> Option<Self::Item> {
        self.first.take().or_else(|| self.remaining.next())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        if self.first.is_some() {
            let length = self.remaining.len() + 1;
            (length, Some(length))
        } else {
            self.remaining.size_hint()
        }
    }
}

impl DoubleEndedIterator for IterMut<'_> {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.remaining.next_back().or_else(|| self.first.take())
    }
}

impl ExactSizeIterator for IterMut<'_> {}

impl FusedIterator for IterMut<'_> {}
