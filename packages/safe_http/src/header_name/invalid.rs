use std::{error::Error, fmt};

#[derive(Debug, Clone)]
#[cfg_attr(test, derive(PartialEq))]
pub struct InvalidHeaderName {
    pub(crate) byte: u8,
}

impl fmt::Display for InvalidHeaderName {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "invalid header name char {:?}", char::from(self.byte),)
    }
}

impl Error for InvalidHeaderName {}
