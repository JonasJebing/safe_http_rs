use std::{error::Error, fmt};

#[derive(Debug, Clone)]
pub struct InvalidMethod {
    pub byte: u8,
}

impl fmt::Display for InvalidMethod {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "invalid http method char {:?}", char::from(self.byte),)
    }
}

impl Error for InvalidMethod {}
