// This file is automatically generated.

use shared_bytes::SharedStr;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub(super) enum MethodKind {
    Get,
    Put,
    Post,
    Delete,
    Options,
    Head,
    Trace,
    Connect,
    Patch,
    Extension(SharedStr),
}

impl MethodKind {
    pub(super) fn standard(s: &str) -> Option<Self> {
        match s {
            "GET" => Some(Self::Get),
            "PUT" => Some(Self::Put),
            "POST" => Some(Self::Post),
            "DELETE" => Some(Self::Delete),
            "OPTIONS" => Some(Self::Options),
            "HEAD" => Some(Self::Head),
            "TRACE" => Some(Self::Trace),
            "CONNECT" => Some(Self::Connect),
            "PATCH" => Some(Self::Patch),
            _ => None,
        }
    }
}

impl MethodKind {
    pub(super) fn as_str(&self) -> &str {
        match self {
            Self::Get => "GET",
            Self::Put => "PUT",
            Self::Post => "POST",
            Self::Delete => "DELETE",
            Self::Options => "OPTIONS",
            Self::Head => "HEAD",
            Self::Trace => "TRACE",
            Self::Connect => "CONNECT",
            Self::Patch => "PATCH",
            Self::Extension(s) => s,
        }
    }
}
