use std::{error::Error, fmt};

#[derive(Debug, Clone)]
#[cfg_attr(test, derive(PartialEq))]
pub struct InvalidHeaderValue {
    pub(crate) byte: u8,
}

impl fmt::Display for InvalidHeaderValue {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "invalid header value char {:?}", char::from(self.byte),)
    }
}

impl Error for InvalidHeaderValue {}
