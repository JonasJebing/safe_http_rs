use super::InvalidHeaderValue;

pub(super) const fn validate(bytes: &[u8]) -> Result<(), InvalidHeaderValue> {
    let mut index = 0;
    while index < bytes.len() {
        let byte = bytes[index];
        let invalid = Err(InvalidHeaderValue { byte });
        if index == 0 || index == bytes.len() - 1 {
            if !is_visible_or_obsolete_text(byte) {
                return invalid;
            }
        } else if !is_rfc_content(byte) {
            return invalid;
        }
        index += 1;
    }
    Ok(())
}

/// roughly equivalent to "field-content" in RFC 7230
const fn is_rfc_content(byte: u8) -> bool {
    is_visible_or_obsolete_text(byte) || is_rfc_whitespace(byte)
}

const fn is_visible_or_obsolete_text(b: u8) -> bool {
    is_rfc_visible_char(b) || is_rfc_obsolete_text(b)
}

/// "VCHAR" in RFC 5234
const fn is_rfc_visible_char(b: u8) -> bool {
    matches!(b, 33..=126)
}

/// "obs-text" in RFC 7230
const fn is_rfc_obsolete_text(b: u8) -> bool {
    matches!(b, 128..=255)
}

/// "OWS", "RWS" or "BWS" in RFC 7230
const fn is_rfc_whitespace(b: u8) -> bool {
    matches!(b, b' ' | b'\t')
}
