use crate::BodyBox;
use safe_http::ResponseHead;

#[derive(Debug, Default)]
pub struct Response {
    pub head: ResponseHead,
    pub body: BodyBox,
    #[doc(hidden)]
    pub __private: (),
}
