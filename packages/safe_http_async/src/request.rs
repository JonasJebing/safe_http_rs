use crate::BodyBox;
use safe_http::RequestHead;

#[derive(Debug, Default)]
pub struct Request {
    pub head: RequestHead,
    pub body: BodyBox,
    #[doc(hidden)]
    pub __private: (),
}
