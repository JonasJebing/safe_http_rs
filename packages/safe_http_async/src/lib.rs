#![forbid(unsafe_code)]

mod body;
mod request;
mod response;

pub use self::{
    body::{Body, BodyBox},
    request::Request,
    response::Response,
};
pub use safe_http::{
    header_map, header_values, HeaderMap, HeaderName, HeaderValue, HeaderValues, InvalidHeaderName,
    InvalidHeaderValue, InvalidStatusCode, Method, RequestHead, RequestLine, ResponseHead,
    ResponseLine, StatusCode, Version,
};

#[cfg(test)]
fn async_test<F, Fut>(f: F)
where
    F: FnOnce() -> Fut,
    Fut: std::future::Future<Output = ()>,
{
    futures::executor::block_on(f())
}
