use super::test_body::TestBody;
use crate::BodyBox;

#[test]
fn qc_read_chunk_to_end() {
    quickcheck::quickcheck(test_read_chunk_to_end as fn(_))
}

fn test_read_chunk_to_end(chunks: Vec<Vec<u8>>) {
    crate::async_test(|| async {
        let mut body = BodyBox::new(TestBody::from(&chunks));
        for expected_chunk in chunks {
            let mut actual_chunk = Vec::new();
            let actual_read_bytes = body.read_chunk_to_end(&mut actual_chunk).await.unwrap();
            assert_eq!(expected_chunk, actual_chunk);
            assert_eq!(expected_chunk.len(), actual_read_bytes);
        }
    })
}

#[test]
fn read_chunk_to_end_two_small_chunks() {
    test_read_chunk_to_end(vec![vec![1], vec![2]])
}

#[test]
fn qc_read_vec() {
    quickcheck::quickcheck(test_read_vec as fn(_))
}

fn test_read_vec(input_chunks: Vec<Vec<u8>>) {
    crate::async_test(|| async {
        let chunks = push_empty_chunks(input_chunks);
        let body = BodyBox::new(TestBody::from(&chunks));
        let actual_bytes = body.read_into_vec().await.unwrap();
        let expected_bytes = Vec::from_iter(chunks.iter().flat_map(|c| c.iter().copied()));
        assert_eq!(actual_bytes, expected_bytes);
    })
}

fn push_empty_chunks(mut chunks: Vec<Vec<u8>>) -> Vec<Vec<u8>> {
    for chunk in &mut chunks {
        if chunk.is_empty() {
            chunk.push(1)
        }
    }
    chunks
}

#[test]
fn read_vec_empty_chunks() {
    test_read_vec(Vec::new())
}

#[test]
fn read_vec_some_chunks() {
    test_read_vec(vec![vec![0, 1, 2, 3], vec![4, 5, 6]])
}

#[test]
fn empty_read_vec() {
    crate::async_test(|| async {
        let actual = BodyBox::empty().read_vec().await.unwrap();
        assert_eq!(actual, Vec::new())
    })
}
