mod box_;
#[cfg(test)]
mod test_body;
#[cfg(test)]
mod tests;
mod trait_;

pub use self::{box_::BodyBox, trait_::Body};
