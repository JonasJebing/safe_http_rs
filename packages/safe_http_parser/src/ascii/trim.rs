use crate::chunks::ChunksSlice;

pub(crate) fn trim(slice: ChunksSlice, mut is_whitespace: impl FnMut(u8) -> bool) -> ChunksSlice {
    let (start, _) = match slice
        .bytes_indexed()
        .find(|&(_, byte)| !is_whitespace(byte))
    {
        Some(x) => x,
        None => return ChunksSlice::empty(),
    };
    match slice
        .bytes_indexed()
        .rev()
        .take_while(|&(_, byte)| is_whitespace(byte))
        .last()
    {
        Some((end, _)) => slice.index(start..end),
        None => slice.index(start..),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use shared_bytes::SharedBytes;

    #[test]
    fn leading_space() {
        let chunks = to_chunks(b" a");
        let slice = ChunksSlice::new(&chunks);
        let actual = Vec::from_iter(trim(slice, is_space).bytes());
        assert_eq!(actual, b"a");
    }

    #[test]
    fn trailing_space() {
        let chunks = to_chunks(b"a ");
        let slice = ChunksSlice::new(&chunks);
        let actual = Vec::from_iter(trim(slice, is_space).bytes());
        assert_eq!(actual, b"a");
    }

    #[test]
    fn leading_and_trailing_space() {
        let chunks = to_chunks(b" a ");
        let slice = ChunksSlice::new(&chunks);
        let actual = Vec::from_iter(trim(slice, is_space).bytes());
        assert_eq!(actual, b"a");
    }

    fn to_chunks(bytes: &'static [u8]) -> [SharedBytes; 1] {
        [bytes.into()]
    }

    fn is_space(b: u8) -> bool {
        matches!(b, b' ')
    }
}
