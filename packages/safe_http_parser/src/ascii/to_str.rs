use crate::{chunks::ChunksSlice, error::Error};
use shared_bytes::SharedStr;
use std::fmt;

pub(crate) fn str_from_ascii(ascii: ChunksSlice) -> Result<SharedStr, Error> {
    for (index, byte) in ascii.bytes().enumerate() {
        if !byte.is_ascii() {
            return Err(Error::Ascii(AsciiError { byte, index }));
        }
    }
    Ok(SharedStr::from_utf8(ascii.to_continuous_shared()).expect("ascii is valid utf-8"))
}

#[derive(Debug, Clone)]
pub(crate) struct AsciiError {
    byte: u8,
    index: usize,
}

impl std::error::Error for AsciiError {}

impl fmt::Display for AsciiError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "byte {} ('{}') at index {} is not an ASCII char",
            self.byte,
            char::from(self.byte),
            self.index
        )
    }
}
