use super::{headers::parse_header_map, version::version_from_bytes};
use crate::{
    ascii::str_from_ascii, chunks::ChunksSlice, error::Error, windows::IteratorExt,
    ParseRequestError, Parsed,
};
use safe_http::{Method, RequestHead, RequestLine, Version};
use safe_uri::{Scheme, Uri, UriRef};
use shared_bytes::SharedBytes;

pub fn parse_request_head(
    chunks: &[SharedBytes],
) -> Result<Parsed<RequestHead>, ParseRequestError> {
    let slice = ChunksSlice::new(chunks);
    let (line, remainder) = parse_request_line(slice).map_err(ParseRequestError)?;
    let (headers, remainder) = parse_header_map(remainder).map_err(ParseRequestError)?;
    Ok(Parsed {
        value: RequestHead {
            line,
            headers,
            ..Default::default()
        },
        remainder: remainder.to_continuous_shared(),
    })
}

fn parse_request_line(slice: ChunksSlice) -> Result<(RequestLine, ChunksSlice), Error> {
    let (method, remainder) = parse_method(slice)?;
    let (uri, remainder) = parse_request_target(remainder)?;
    let (version, remainder) = parse_version(remainder)?;
    let line = RequestLine {
        method,
        uri,
        version,
        ..Default::default()
    };
    Ok((line, remainder))
}

fn parse_method(slice: ChunksSlice) -> Result<(Method, ChunksSlice), Error> {
    let method_end = slice
        .bytes_indexed()
        .find_map(|(index, byte)| (byte == b' ').then(|| index))
        .ok_or(Error::Message("missing method separator ' '"))?;
    let method = method_from_slice(slice.index(..method_end))?;
    let remainder_start = slice.next_chunks_index(method_end).ok_or(Error::Message(
        "no request target (URI) after separator ' '",
    ))?;
    let remainder = slice.index(remainder_start..);
    Ok((method, remainder))
}

fn method_from_slice(slice: ChunksSlice) -> Result<Method, Error> {
    let string = str_from_ascii(slice)?;
    string.try_into().map_err(Error::Method)
}

fn parse_request_target(slice: ChunksSlice) -> Result<(Uri, ChunksSlice), Error> {
    let uri_end = slice
        .bytes_indexed()
        .find_map(|(index, byte)| (byte == b' ').then(|| index))
        .ok_or(Error::Message("missing uri separator ' '"))?;
    let uri = uri_from_slice(slice.index(..uri_end))?;
    let remainder_start = slice
        .next_chunks_index(uri_end)
        .ok_or(Error::Message("no HTTP version after separator ' '"))?;
    let remainder = slice.index(remainder_start..);
    Ok((uri, remainder))
}

fn uri_from_slice(slice: ChunksSlice) -> Result<Uri, Error> {
    let string = str_from_ascii(slice)?;
    let uri_ref = UriRef::parse(string).map_err(Error::Uri)?;
    Ok(uri_ref.into_uri_with_default_scheme(|| Scheme::HTTPS))
}

fn parse_version(slice: ChunksSlice) -> Result<(Version, ChunksSlice), Error> {
    let [version_end, line_end] = slice
        .bytes_indexed()
        .windows::<2>()
        .find_map(|[(index0, byte0), (index1, byte1)]| {
            ([byte0, byte1] == *b"\r\n").then(|| [index0, index1])
        })
        .ok_or(Error::Message("missing version separator (CRLF)"))?;
    let version = version_from_bytes(&slice.index(..version_end).to_continuous_cow())?;
    let remainder_start = slice
        .next_chunks_index(line_end)
        .ok_or(Error::Message("no bytes following the request line"))?;
    let remainder = slice.index(remainder_start..);
    Ok((version, remainder))
}

#[cfg(test)]
mod tests {
    use super::*;
    use safe_http::{HeaderMap, HeaderName, HeaderValue, Version};
    use safe_uri::Path;
    use tap::Tap;

    #[test]
    fn good_case() {
        let request = "PUT /example HTTP/2.0\r\n\
            content-type: text/plain\r\n\
            content-length: 5\r\n\r\n\
            hello";
        let bytes = SharedBytes::from(request);
        let parsed_request_head = parse_request_head(&[bytes]).unwrap();
        let expected_head = RequestHead {
            line: RequestLine {
                method: Method::PUT,
                uri: Uri::new().tap_mut(|u| {
                    u.scheme = Scheme::HTTPS;
                    u.resource.path = Path::from_static("/example");
                }),
                version: Version::HTTP_2_0,
                ..Default::default()
            },
            headers: HeaderMap::from([
                (
                    HeaderName::CONTENT_TYPE,
                    HeaderValue::from_static(b"text/plain"),
                ),
                (HeaderName::CONTENT_LENGTH, HeaderValue::from_static(b"5")),
            ]),
            ..Default::default()
        };
        assert_eq!(parsed_request_head.value, expected_head);
        assert_eq!(parsed_request_head.remainder, "hello");
    }
}
