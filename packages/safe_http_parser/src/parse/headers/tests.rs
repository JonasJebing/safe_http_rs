use super::*;
use shared_bytes::SharedBytes;
use std::fmt::Debug;

#[test]
fn good_case() {
    let string = "\
            Content-Type: text/plain\r\n\
            content-length: 0\r\n\
        ";
    let chunks = [SharedBytes::from(string)];
    let slice = ChunksSlice::new(&chunks);
    assert_eq!(
        parse_header_map(slice).unwrap().0,
        header_map(&[
            (HeaderName::CONTENT_TYPE, "text/plain"),
            (HeaderName::CONTENT_LENGTH, "0"),
        ])
    );
}

fn header_map<K>(slice: &[(K, &'static str)]) -> HeaderMap
where
    K: TryInto<HeaderName> + Clone,
    K::Error: Debug,
{
    slice
        .iter()
        .map(|(k, v)| {
            (
                k.clone().try_into().unwrap(),
                HeaderValue::try_from(v.as_bytes()).unwrap(),
            )
        })
        .collect()
}
