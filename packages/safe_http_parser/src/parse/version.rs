use crate::error::Error;
use safe_http::Version;

pub(super) fn version_from_bytes(prefixed_bytes: &[u8]) -> Result<Version, Error> {
    let bytes = prefixed_bytes
        .strip_prefix(b"HTTP/")
        .ok_or(Error::Message("missing http version prefix"))?;
    let unknown_version = Err(Error::Message("unknown or unsupported http version"));
    Ok(match bytes {
        b"2.0" => Version::HTTP_2_0,
        b"1.1" => Version::HTTP_1_1,
        b"1.0" => Version::HTTP_1_0,
        _ => return unknown_version,
    })
}
