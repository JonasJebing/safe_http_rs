#[cfg(test)]
mod tests;

use crate::{
    ascii::str_from_ascii,
    chunks::{ChunksIndex, ChunksSlice},
    error::Error,
    windows::IteratorExt,
};
use safe_http::{HeaderMap, HeaderName, HeaderValue};

const CR: u8 = b'\r';
const LF: u8 = b'\n';

pub(crate) fn parse_header_map(slice: ChunksSlice) -> Result<(HeaderMap, ChunksSlice), Error> {
    let mut headers = HeaderMap::new();
    let mut remaining = slice;
    loop {
        if remaining.is_empty() {
            return Ok((headers, remaining));
        }
        if let Some(remaining) = skip_cr_lf(remaining) {
            return Ok((headers, remaining));
        }
        let (name, r) = next_name(remaining)?;
        remaining = r;
        let (value, r) = next_value(remaining)?;
        remaining = r;
        headers.append(name, value);
        match skip_cr_lf(remaining) {
            // this CRLF is the header entry separator
            Some(r) => remaining = r,
            None => {
                return if remaining.is_empty() {
                    Ok((headers, remaining))
                } else {
                    Err(Error::Message(
                        r#"missing header entry separator "\r\n" (CRLF)"#,
                    ))
                }
            }
        }
    }
}

fn skip_cr_lf(slice: ChunksSlice) -> Option<ChunksSlice> {
    let (indexes, bytes) = slice
        .bytes_indexed()
        .windows::<2>()
        .map(separate_window)
        .next()?;
    match &bytes {
        b"\r\n" => Some(slice.split_at(indexes[1]).1),
        _ => None,
    }
}

fn next_name(slice: ChunksSlice) -> Result<(HeaderName, ChunksSlice), Error> {
    let name_end = slice
        .bytes_indexed()
        .find_map(|(index, byte)| (byte == b':').then(|| index))
        .ok_or(Error::Message("missing header field separator ':'"))?;
    let name = try_name_from(slice.index(..name_end))?;
    let remainder_start = slice
        .next_chunks_index(name_end)
        .ok_or(Error::Message("no header value after separator ':'"))?;
    let remainder = slice.index(remainder_start..);
    Ok((name, remainder))
}

fn next_value(slice: ChunksSlice) -> Result<(HeaderValue, ChunksSlice), Error> {
    let end = next_value_end(slice)?;
    let value_slice = slice.index(..end);
    let value = try_value_from(value_slice)?;
    let remainder = slice.index(end..);
    Ok((value, remainder))
}

fn next_value_end(slice: ChunksSlice) -> Result<ChunksIndex, Error> {
    let mut last_window = None;
    for (indexes, bytes) in slice.bytes_indexed().windows::<3>().map(separate_window) {
        last_window = Some((indexes, bytes));
        if let [CR, LF, last_byte] = bytes {
            return if is_rfc_whitespace(last_byte) {
                Err(Error::Message("obsolete line folding (obs-fold: CRLF followed by spaces or tabs) is not supported"))
            } else {
                Ok(indexes[0])
            };
        }
    }
    if let Some((indexes, [_, CR, LF])) = last_window {
        return Ok(indexes[1]);
    }
    Ok(slice.end_chunks_index())
}

fn separate_window<const N: usize>(window: [(ChunksIndex, u8); N]) -> ([ChunksIndex; N], [u8; N]) {
    (window.map(|x| x.0), window.map(|x| x.1))
}

fn is_rfc_whitespace(b: u8) -> bool {
    matches!(b, b' ' | b'\t')
}

fn trim_ascii(slice: ChunksSlice) -> ChunksSlice {
    crate::ascii::trim(slice, is_rfc_whitespace)
}

fn try_name_from(slice: ChunksSlice) -> Result<HeaderName, Error> {
    let string = str_from_ascii(slice)?;
    string.try_into().map_err(Error::HeaderName)
}

fn try_value_from(slice: ChunksSlice) -> Result<HeaderValue, Error> {
    trim_ascii(slice)
        .to_continuous_shared()
        .try_into()
        .map_err(Error::HeaderValue)
}
