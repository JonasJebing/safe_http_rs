use crate::ascii::AsciiError;
use safe_http::{InvalidHeaderName, InvalidHeaderValue, InvalidMethod, InvalidStatusCode};
use safe_uri::ParseUriError;
use std::{error, fmt, num::ParseIntError};

#[derive(Debug, Clone)]
pub(crate) enum Error {
    Ascii(AsciiError),
    HeaderName(InvalidHeaderName),
    HeaderValue(InvalidHeaderValue),
    Message(&'static str),
    Method(InvalidMethod),
    Uri(ParseUriError),
    StatusCodeParse(ParseIntError),
    StatusCode(InvalidStatusCode),
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        use Error::*;
        Some(match self {
            Message(_) => return None,
            Ascii(s) => s,
            HeaderName(s) => s,
            HeaderValue(s) => s,
            Method(s) => s,
            Uri(s) => s,
            StatusCodeParse(s) => s,
            StatusCode(s) => s,
        })
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Error::*;
        match self {
            Ascii(_) => f.write_str("invalid ASCII"),
            HeaderName(_) => f.write_str("invalid header name"),
            HeaderValue(_) => f.write_str("invalid header value"),
            Message(m) => f.write_str(m),
            Method(_) => f.write_str("invalid method"),
            Uri(_) => f.write_str("failed to parse URI"),
            StatusCode(_) | StatusCodeParse(_) => f.write_str("failed status code"),
        }
    }
}

#[derive(Debug, Clone)]
pub struct ParseRequestError(pub(crate) Error);

impl error::Error for ParseRequestError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        Some(&self.0)
    }
}

impl fmt::Display for ParseRequestError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("failed to parse HTTP request")
    }
}

#[derive(Debug, Clone)]
pub struct ParseResponseError(pub(crate) Error);

impl error::Error for ParseResponseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        Some(&self.0)
    }
}

impl fmt::Display for ParseResponseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("failed to parse HTTP response")
    }
}
