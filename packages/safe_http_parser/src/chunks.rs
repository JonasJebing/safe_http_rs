mod index;
mod slice;

pub(crate) use self::{index::ChunksIndex, slice::ChunksSlice};

use shared_bytes::SharedBytes;

pub(crate) type Chunks<'a> = &'a [SharedBytes];
