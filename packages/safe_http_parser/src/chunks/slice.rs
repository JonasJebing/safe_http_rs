mod index;
mod iter;

use self::index::ChunksSliceIndex;
use super::{Chunks, ChunksIndex};
use shared_bytes::SharedBytes;
use std::{borrow::Cow, fmt::Debug};

pub(crate) use iter::BytesIndexed;

#[derive(Debug, Clone, Copy)]
pub(crate) struct ChunksSlice<'a> {
    chunks: Chunks<'a>,

    /// inclusive start in the first chunk
    start: usize,

    /// exclusive end in the last chunk
    end: usize,
}

impl<'a> From<Chunks<'a>> for ChunksSlice<'a> {
    fn from(chunks: Chunks<'a>) -> Self {
        Self::new(chunks)
    }
}

impl<'a> ChunksSlice<'a> {
    pub fn new(chunks: Chunks<'a>) -> Self {
        Self {
            chunks,
            start: 0,
            end: chunks.last().map(|chunk| chunk.len()).unwrap_or(0),
        }
    }

    pub fn empty() -> Self {
        Self {
            chunks: &[],
            start: 0,
            end: 0,
        }
    }

    pub fn get<I: ChunksSliceIndex<Self>>(self, index: I) -> Option<I::Output> {
        index.get(self)
    }

    pub fn index<I: ChunksSliceIndex<Self>>(self, index: I) -> I::Output {
        index.get(self).unwrap()
    }

    pub fn bytes_indexed(self) -> BytesIndexed<'a> {
        BytesIndexed::new(self)
    }

    pub fn bytes(
        self,
    ) -> impl DoubleEndedIterator<Item = u8> + ExactSizeIterator + Debug + Clone + 'a {
        self.bytes_indexed().map(|(_index, byte)| byte)
    }

    #[allow(dead_code)]
    pub fn indexes(
        self,
    ) -> impl DoubleEndedIterator<Item = ChunksIndex> + ExactSizeIterator + Debug + Clone + 'a {
        self.bytes_indexed().map(|(index, _byte)| index)
    }

    pub fn split_at(self, index: ChunksIndex) -> (Self, Self) {
        self.try_split_at(index).unwrap()
    }

    pub fn try_split_at(self, index: ChunksIndex) -> Option<(Self, Self)> {
        let first = self.get(..index)?;
        let second = self
            .next_chunks_index(index)
            .and_then(|next_index| self.get(next_index..))
            .unwrap_or_else(Self::empty);
        Some((first, second))
    }

    pub fn next_chunks_index(self, index: ChunksIndex) -> Option<ChunksIndex> {
        let slice = self;
        let chunks = slice.chunks;
        let adjusted = self.adjust_chunks_index(index);
        let next_byte_index = index.byte.checked_add(1)?;
        let next_adjusted_byte_index = adjusted.byte.checked_add(1)?;
        let chunk = chunks.get(adjusted.chunk)?;
        if next_adjusted_byte_index < chunk.len() {
            Some(ChunksIndex {
                chunk: index.chunk,
                byte: next_byte_index,
            })
        } else {
            for next_chunk_index in index.chunk.checked_add(1)?.. {
                let next_chunk = chunks.get(next_chunk_index)?;
                if !next_chunk.is_empty() {
                    return Some(ChunksIndex {
                        chunk: next_chunk_index,
                        byte: 0,
                    });
                }
            }
            None
        }
    }

    pub fn previous_chunks_index(self, index: ChunksIndex) -> Option<ChunksIndex> {
        let slice = self;
        let chunks = slice.chunks;
        if let Some(previous_byte_index) = index.byte.checked_sub(1) {
            let adjusted = self.adjust_chunks_index(index);
            let previous_adjusted_byte_index = adjusted.byte - 1;
            let chunk = chunks.get(adjusted.chunk)?;
            (previous_adjusted_byte_index < chunk.len()).then(|| ChunksIndex {
                chunk: index.chunk,
                byte: previous_byte_index,
            })
        } else {
            for previous_chunk_index in
                std::iter::successors(index.chunk.checked_sub(1), |i| i.checked_sub(1))
            {
                let previous_chunk = chunks.get(previous_chunk_index)?;
                if previous_chunk.is_empty() {
                    continue;
                }
                let previous_byte_index = if previous_chunk_index == 0 {
                    previous_chunk.len() - 1 - self.start
                } else {
                    previous_chunk.len() - 1
                };
                return Some(ChunksIndex {
                    chunk: previous_chunk_index,
                    byte: previous_byte_index,
                });
            }
            None
        }
    }

    pub fn to_continuous_shared(self) -> SharedBytes {
        match self.chunks.len() {
            0 => SharedBytes::new(),
            1 => self.chunks[0].slice_cloned(self.start..self.end),
            _ => self.bytes().collect(),
        }
    }

    pub fn to_continuous_cow(self) -> Cow<'a, [u8]> {
        match self.chunks.len() {
            0 => Cow::Owned(Vec::new()),
            1 => Cow::Borrowed(&self.chunks[0][self.start..self.end]),
            _ => self.bytes().collect(),
        }
    }

    pub fn bytes_length(self) -> usize {
        let first_plus_last = || {
            self.chunks
                .first()
                .unwrap()
                .len()
                .saturating_sub(self.start)
                + self.end
        };
        match self.chunks.len() {
            0 => 0,
            1 => self.end.saturating_sub(self.start),
            2 => first_plus_last(),
            chunks_length => {
                first_plus_last()
                    + self.chunks[1..chunks_length - 1]
                        .iter()
                        .map(|c| c.len())
                        .sum::<usize>()
            }
        }
    }

    pub fn is_empty(self) -> bool {
        self.bytes_length() == 0
    }

    pub fn end_chunks_index(self) -> ChunksIndex {
        let chunk = self.chunks.len().saturating_sub(1);
        if self.chunks.len() <= 1 {
            ChunksIndex {
                chunk,
                byte: self.end - self.start,
            }
        } else {
            ChunksIndex {
                chunk,
                byte: self.end,
            }
        }
    }

    pub fn start_chunks_index(self) -> ChunksIndex {
        for (chunk_index, chunk) in self.chunks.iter().enumerate() {
            if !chunk.is_empty() {
                return ChunksIndex {
                    chunk: chunk_index,
                    byte: 0,
                };
            }
        }
        ChunksIndex::MIN
    }

    fn adjust_chunks_index(self, index: ChunksIndex) -> ChunksIndex {
        if index.chunk == 0 {
            ChunksIndex {
                chunk: index.chunk,
                byte: self.start + index.byte,
            }
        } else {
            index
        }
    }

    fn contains_adjusted_index(self, index: ChunksIndex) -> bool {
        self.internal_contains_adjusted_index(index, |index, end| index < end)
    }

    fn is_valid_adjusted_end_index(self, index: ChunksIndex) -> bool {
        self.internal_contains_adjusted_index(index, |index, end| index <= end)
    }

    fn internal_contains_adjusted_index(
        self,
        index: ChunksIndex,
        check_end: impl Fn(usize, usize) -> bool,
    ) -> bool {
        if self.chunks.is_empty() {
            return false;
        }
        let chunk = match self.chunks.get(index.chunk) {
            None => return false,
            Some(c) => c,
        };
        if !check_end(index.byte, chunk.len()) {
            return false;
        }
        if index.chunk == 0 && index.byte < self.start {
            return false;
        }
        let last_chunks_index = self.chunks.len() - 1; // never overflows because chunks is not empty
        if index.chunk == last_chunks_index && !check_end(index.byte, self.end) {
            return false;
        }
        true
    }
}

#[cfg(test)]
#[derive(Debug, Clone)]
struct OwnedChunksSlice {
    chunks: Vec<SharedBytes>,

    /// inclusive start in the first chunk
    start: usize,

    /// exclusive end in the last chunk
    end: usize,
}

#[cfg(test)]
use quickcheck::{Arbitrary, Gen};

#[cfg(test)]
impl Arbitrary for OwnedChunksSlice {
    fn arbitrary(g: &mut Gen) -> Self {
        let chunks: Vec<ArbitrarySharedBytes> = Arbitrary::arbitrary(g);
        Self {
            chunks: chunks.into_iter().map(|c| c.0).collect(),
            start: Arbitrary::arbitrary(g),
            end: Arbitrary::arbitrary(g),
        }
        .into_normalized()
    }

    fn shrink(&self) -> Box<dyn Iterator<Item = Self>> {
        let start = self.start;
        let end = self.end;
        let chunks: Vec<ArbitrarySharedBytes> = self
            .chunks
            .iter()
            .map(|c| ArbitrarySharedBytes(c.clone()))
            .collect();
        Box::new(chunks.shrink().map(move |chunks| {
            OwnedChunksSlice {
                chunks: chunks.into_iter().map(|c| c.0).collect(),
                start,
                end,
            }
            .into_normalized()
        }))
    }
}

#[cfg(test)]
#[derive(Debug, Clone)]
struct ArbitrarySharedBytes(SharedBytes);

#[cfg(test)]
impl Arbitrary for ArbitrarySharedBytes {
    fn arbitrary(g: &mut Gen) -> Self {
        Self(SharedBytes::from_vec(Arbitrary::arbitrary(g)))
    }

    fn shrink(&self) -> Box<dyn Iterator<Item = Self>> {
        Box::new(self.0.to_vec().shrink().map(|v| Self(v.into())))
    }
}

#[cfg(test)]
impl OwnedChunksSlice {
    fn into_normalized(self) -> Self {
        let chunks = self.chunks;
        let start = self.start.min(
            chunks
                .first()
                .map(|c| c.len().saturating_sub(1))
                .unwrap_or(0),
        );
        let end_min = if chunks.len() <= 1 { start } else { 0 };
        let end = self
            .end
            .clamp(end_min, chunks.last().map(|c| c.len()).unwrap_or(0));
        Self { chunks, start, end }
    }

    fn to_borrowed(&self) -> ChunksSlice {
        ChunksSlice {
            chunks: &self.chunks,
            start: self.start,
            end: self.end,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn chunks<const N: usize>(vecs: [Vec<u8>; N]) -> [SharedBytes; N] {
        vecs.map(Into::into)
    }

    #[test]
    fn new() {
        let chunks = chunks([vec![0, 1], vec![0, 1]]);
        let slice = ChunksSlice::new(&chunks);
        assert_eq!(slice.start, 0);
        assert_eq!(slice.end, 2);
    }

    #[test]
    fn next_chunks_index_with_empty_chunks() {
        let chunks = chunks([vec![0, 1], Vec::new(), Vec::new(), Vec::new(), vec![0, 1]]);
        let slice = ChunksSlice {
            chunks: &chunks,
            start: 1,
            end: 1,
        };
        assert_eq!(
            slice.next_chunks_index(ChunksIndex { chunk: 0, byte: 0 }),
            Some(ChunksIndex { chunk: 4, byte: 0 })
        );
    }

    #[test]
    fn bytes_length_for_two_chunks() {
        let chunks = chunks([vec![0, 1], vec![0, 1]]);
        let slice = ChunksSlice {
            chunks: &chunks,
            start: 1,
            end: 1,
        };
        assert_eq!(slice.bytes_length(), 2);
    }

    #[test]
    fn bytes_length_for_one_chunk() {
        let chunks = chunks([vec![0]]);
        let slice = ChunksSlice::new(&chunks);
        assert_eq!(slice.start, 0);
        assert_eq!(slice.end, 1);
        assert_eq!(slice.bytes_length(), 1);
    }

    #[test]
    fn start_chunks_index_first_two_chunks_empty() {
        let chunks = chunks([vec![], vec![], vec![0]]);
        let slice = ChunksSlice::new(&chunks);
        assert_eq!(
            slice.start_chunks_index(),
            ChunksIndex { chunk: 2, byte: 0 }
        );
    }

    #[test]
    fn start_chunks_index_second_chunk_empty() {
        let chunks = chunks([vec![0], vec![]]);
        let slice = ChunksSlice::new(&chunks);
        assert_eq!(
            slice.start_chunks_index(),
            ChunksIndex { chunk: 0, byte: 0 }
        );
    }

    #[test]
    fn start_chunks_index_all_empty() {
        let chunks = chunks([vec![], vec![], vec![]]);
        let slice = ChunksSlice::new(&chunks);
        assert_eq!(slice.start_chunks_index(), ChunksIndex::MIN);
    }
}
