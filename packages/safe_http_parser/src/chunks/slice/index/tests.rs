use super::ChunksSliceIndex;
use crate::chunks::{ChunksIndex, ChunksSlice};
use shared_bytes::SharedBytes;

fn chunks<const N: usize>(vecs: [Vec<u8>; N]) -> [SharedBytes; N] {
    vecs.map(Into::into)
}

mod chunks_index {
    use super::*;

    #[test]
    fn upper_out_of_bounds() {
        let chunks = chunks([vec![0, 1]]);
        let slice = ChunksSlice {
            chunks: &chunks,
            start: 0,
            end: 1,
        };
        let actual = ChunksIndex { chunk: 0, byte: 1 }.get(slice);
        assert_eq!(actual, None);
    }
}

mod range_chunks_index {
    use super::*;

    #[test]
    fn around_separated_chunks() {
        let chunks = chunks([vec![0, 1], vec![2, 3]]);
        let slice = ChunksSlice::new(&chunks);
        let start = ChunksIndex { chunk: 0, byte: 1 };
        let end = ChunksIndex { chunk: 1, byte: 1 };
        let actual = (start..end).get(slice).unwrap();
        assert_eq!(actual.start, 1);
        assert_eq!(actual.end, 1);
        let actual_bytes = Vec::from_iter(actual.bytes());
        assert_eq!(actual_bytes, vec![1, 2]);
    }

    #[test]
    fn single_byte_slice_no_op() {
        let chunks = chunks([vec![0]]);
        let slice = ChunksSlice::new(&chunks);
        let start = ChunksIndex { chunk: 0, byte: 0 };
        let end = ChunksIndex { chunk: 0, byte: 1 };
        let actual = (start..end).get(slice).unwrap();
        assert_eq!(actual.chunks, &chunks);
        assert_eq!(actual.start, 0);
        assert_eq!(actual.end, 1);
    }
}

mod range_to_chunks_index {
    use super::*;

    #[test]
    fn single_chunk() {
        let chunks = chunks([vec![0, 1, 2, 3, 4]]);
        let slice = ChunksSlice {
            chunks: &chunks,
            start: 1,
            end: 4,
        };
        let end = ChunksIndex { chunk: 0, byte: 2 };
        let actual = slice.get(..end).unwrap();
        assert_eq!(actual.end, 3);
    }

    #[test]
    fn one_empty_chunk() {
        let chunks = chunks([vec![], vec![0, 1, 2]]);
        let slice = ChunksSlice {
            chunks: &chunks,
            start: 0,
            end: 2,
        };
        let end = ChunksIndex { chunk: 1, byte: 1 };
        let actual = slice.get(..end).unwrap();
        assert_eq!(actual.end, 1);
    }
}

mod quickcheck_ {
    use super::*;
    use std::iter;

    #[test]
    fn all_chunks_indexes() {
        quickcheck::quickcheck(test_all_chunks_indexes_for_chunk_sizes as fn(Vec<u8>));
    }

    fn test_all_chunks_indexes_for_chunk_sizes(chunk_sizes: Vec<u8>) {
        if let Some(0) | None = chunk_sizes.first() {
            return;
        }
        let chunks = incrementing_byte_chunks_from_chunk_sizes(&chunk_sizes);
        let slice = ChunksSlice::new(&chunks);
        let chunks_indexes =
            iter::successors(Some(ChunksIndex::MIN), |&i| slice.next_chunks_index(i));
        for (index, chunk_index) in chunks_indexes.enumerate() {
            let index = u8::try_from(index).unwrap();
            assert_eq!(chunk_index.get(slice), Some(index));
        }
    }

    fn incrementing_byte_chunks_from_chunk_sizes(chunk_sizes: &[u8]) -> Vec<SharedBytes> {
        let mut iter = 0..u8::MAX;
        Vec::from_iter(
            chunk_sizes
                .iter()
                .map(|&chunk_size| SharedBytes::from_iter((&mut iter).take(chunk_size.into()))),
        )
    }
}
