use super::{ChunksIndex, ChunksSlice};
use std::iter::FusedIterator;

#[derive(Debug, Clone)]
pub(crate) struct BytesIndexed<'a> {
    slice: ChunksSlice<'a>,

    /// inclusive start index
    start: ChunksIndex,

    /// exclusive end index
    end: ChunksIndex,
}

impl<'a> BytesIndexed<'a> {
    pub(super) fn new(slice: ChunksSlice<'a>) -> Self {
        Self {
            slice,
            start: slice.start_chunks_index(),
            end: slice.end_chunks_index(),
        }
    }

    fn length(&self) -> usize {
        match self.slice.get(self.start..self.end) {
            Some(slice) => slice.bytes_length(),
            None => 0,
        }
    }
}

impl<'a> Iterator for BytesIndexed<'a> {
    type Item = (ChunksIndex, u8);

    fn next(&mut self) -> Option<Self::Item> {
        let start = self.start;
        self.start = self
            .slice
            .next_chunks_index(start)
            .unwrap_or(ChunksIndex::MAX);
        Some((start, self.slice.get(start)?))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let length = self.length();
        (length, Some(length))
    }
}

impl ExactSizeIterator for BytesIndexed<'_> {
    fn len(&self) -> usize {
        self.length()
    }
}

impl DoubleEndedIterator for BytesIndexed<'_> {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.end = self.slice.previous_chunks_index(self.end)?;
        Some((self.end, self.slice.get(self.end)?))
    }
}

impl FusedIterator for BytesIndexed<'_> {}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::chunks::slice::OwnedChunksSlice;
    use shared_bytes::SharedBytes;

    fn chunks<const N: usize>(vecs: [Vec<u8>; N]) -> [SharedBytes; N] {
        vecs.map(Into::into)
    }

    #[test]
    fn bounded_single_chunk() {
        let chunks = chunks([vec![0, 1, 2, 3]]);
        let slice = ChunksSlice {
            chunks: &chunks,
            start: 1,
            end: 3,
        };
        let iter = BytesIndexed::new(slice);
        assert_eq!(iter.len(), 2);
        let actual = Vec::from_iter(iter);
        let expected = Vec::from([
            (ChunksIndex { chunk: 0, byte: 0 }, 1),
            (ChunksIndex { chunk: 0, byte: 1 }, 2),
        ]);
        assert_eq!(actual, expected);
    }

    #[test]
    fn bounded_single_chunk_rev() {
        let chunks = chunks([vec![0, 1, 2, 3]]);
        let slice = ChunksSlice {
            chunks: &chunks,
            start: 1,
            end: 3,
        };
        let iter = BytesIndexed::new(slice).rev();
        assert_eq!(iter.len(), 2);
        let actual = Vec::from_iter(iter);
        let expected = Vec::from([
            (ChunksIndex { chunk: 0, byte: 1 }, 2),
            (ChunksIndex { chunk: 0, byte: 0 }, 1),
        ]);
        assert_eq!(actual, expected);
    }

    #[test]
    fn bounded_double_chunk() {
        let chunks = chunks([vec![0, 1], vec![2, 3]]);
        let slice = ChunksSlice {
            chunks: &chunks,
            start: 1,
            end: 1,
        };
        let iter = BytesIndexed::new(slice);
        assert_eq!(iter.len(), 2);
        let actual = Vec::from_iter(iter);
        let expected = Vec::from([
            (ChunksIndex { chunk: 0, byte: 0 }, 1),
            (ChunksIndex { chunk: 1, byte: 0 }, 2),
        ]);
        assert_eq!(actual, expected);
    }

    #[test]
    fn bounded_double_chunk_rev() {
        let chunks = chunks([vec![0, 1], vec![2, 3]]);
        let slice = ChunksSlice {
            chunks: &chunks,
            start: 1,
            end: 1,
        };
        let iter = BytesIndexed::new(slice).rev();
        assert_eq!(iter.len(), 2);
        let actual = Vec::from_iter(iter);
        let expected = Vec::from([
            (ChunksIndex { chunk: 1, byte: 0 }, 2),
            (ChunksIndex { chunk: 0, byte: 0 }, 1),
        ]);
        assert_eq!(actual, expected);
    }

    #[test]
    fn reverse_equals_vec_reverse() {
        quickcheck::quickcheck(test_reverse_equals_vec_reverse as fn(OwnedChunksSlice))
    }

    fn test_reverse_equals_vec_reverse(owned_slice: OwnedChunksSlice) {
        let slice = owned_slice.to_borrowed();
        let iter = BytesIndexed::new(slice);
        let reverse = Vec::from_iter(iter.clone().rev());
        let mut vec_reverse = Vec::from_iter(iter);
        vec_reverse.reverse();
        assert_eq!(reverse, vec_reverse);
    }

    #[test]
    fn reverse_equals_vec_reverse_example_empty_and_0_chunks() {
        test_reverse_equals_vec_reverse(OwnedChunksSlice {
            chunks: vec![vec![].into(), vec![0].into()],
            start: 0,
            end: 1,
        })
    }

    #[test]
    fn length_equals_count() {
        quickcheck::quickcheck(test_length_equals_count as fn(OwnedChunksSlice))
    }

    fn test_length_equals_count(owned_slice: OwnedChunksSlice) {
        let iter = BytesIndexed::new(owned_slice.to_borrowed());
        assert_eq!(iter.len(), iter.count());
    }

    #[test]
    fn length_equals_count_example_single_byte() {
        test_length_equals_count(OwnedChunksSlice {
            chunks: vec![vec![0].into()],
            start: 0,
            end: 1,
        })
    }
}
