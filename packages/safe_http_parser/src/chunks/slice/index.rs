use crate::chunks::{ChunksIndex, ChunksSlice};
use std::ops::{Range, RangeFrom, RangeTo};

#[cfg(test)]
mod tests;

pub(crate) trait ChunksSliceIndex<S> {
    type Output;

    fn get(self, slice: S) -> Option<Self::Output>;
}

impl ChunksSliceIndex<ChunksSlice<'_>> for ChunksIndex {
    type Output = u8;

    fn get(self, slice: ChunksSlice) -> Option<Self::Output> {
        let index = slice.adjust_chunks_index(self);
        if slice.contains_adjusted_index(index) {
            slice.chunks.get(index.chunk)?.get(index.byte).copied()
        } else {
            None
        }
    }
}

impl<'a> ChunksSliceIndex<ChunksSlice<'a>> for Range<ChunksIndex> {
    type Output = ChunksSlice<'a>;

    fn get(self, slice: ChunksSlice<'a>) -> Option<Self::Output> {
        let start = slice.adjust_chunks_index(self.start);
        let end = slice.adjust_chunks_index(self.end);
        if start < end
            && slice.contains_adjusted_index(start)
            && slice.is_valid_adjusted_end_index(end)
        {
            let new_slice = ChunksSlice {
                chunks: slice.chunks.get(start.chunk..=end.chunk)?,
                start: start.byte,
                end: end.byte,
            };
            Some(new_slice)
        } else {
            None
        }
    }
}

impl<'a> ChunksSliceIndex<ChunksSlice<'a>> for RangeFrom<ChunksIndex> {
    type Output = ChunksSlice<'a>;

    fn get(self, slice: ChunksSlice<'a>) -> Option<Self::Output> {
        ChunksSliceIndex::get(self.start..slice.end_chunks_index(), slice)
    }
}

impl<'a> ChunksSliceIndex<ChunksSlice<'a>> for RangeTo<ChunksIndex> {
    type Output = ChunksSlice<'a>;

    fn get(self, slice: ChunksSlice<'a>) -> Option<Self::Output> {
        ChunksSliceIndex::get(slice.start_chunks_index()..self.end, slice)
    }
}
