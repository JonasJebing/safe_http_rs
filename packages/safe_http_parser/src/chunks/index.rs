#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Default)]
pub(crate) struct ChunksIndex {
    /// index of the chunk
    pub chunk: usize,

    /// index of the byte in the chunk
    pub byte: usize,
}

impl ChunksIndex {
    pub(crate) const MIN: Self = Self { chunk: 0, byte: 0 };

    pub const MAX: Self = Self {
        chunk: usize::MAX,
        byte: usize::MAX,
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn less_by_byte() {
        assert!(ChunksIndex { chunk: 0, byte: 1 } < ChunksIndex { chunk: 0, byte: 2 })
    }

    #[test]
    fn less_by_chunk() {
        assert!(ChunksIndex { chunk: 1, byte: 4 } < ChunksIndex { chunk: 2, byte: 3 })
    }
}
