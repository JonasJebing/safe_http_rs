use shared_bytes::SharedBytes;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Parsed<T> {
    pub value: T,
    pub remainder: SharedBytes,
}
