use crate::GENERATED_FILE_COMMENT;
use std::{fs, path::Path};

pub fn generate() {
    let template =
        fs::read_to_string("packages/code_gen/templates/validate_percent_encoded.rs").unwrap();
    let generated_code = format!("{GENERATED_FILE_COMMENT}\n{template}");
    let target_paths = [
        "packages/safe_uri/src/host_name/generated_validate.rs",
        "packages/safe_uri/src/path/generated_validate.rs",
        "packages/safe_uri/src/validation/query_or_fragment/generated_validate.rs",
        "packages/safe_uri/src/user_info/generated_validate.rs",
    ];
    for target_path in target_paths.iter().map(Path::new) {
        fs::create_dir_all(target_path.parent().unwrap()).unwrap();
        fs::write(target_path, &generated_code).unwrap();
    }
}
