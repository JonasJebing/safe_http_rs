use crate::GENERATED_FILE_COMMENT;
use heck::ShoutySnekCase;
use std::fs;

const STATUS_CODES_PATH: &str = "packages/code_gen/data/status_codes.json";
const CONSTANTS_TARGET: &str = "packages/safe_http/src/status/constants.rs";

pub fn generate() {
    let json = fs::read_to_string(STATUS_CODES_PATH).unwrap();
    let status_codes: Vec<JsonStatusCode> = serde_json::from_str(&json).unwrap();
    let constants = gen_constants(&status_codes);
    fs::write(CONSTANTS_TARGET, &constants).unwrap();
}

#[derive(Debug, serde::Deserialize)]
struct JsonStatusCode<'a> {
    value: u16,
    reason_phrase: &'a str,
    rfc: Option<u32>,
    rfc_section: Option<&'a str>,
}

fn gen_constants(status_codes: &[JsonStatusCode]) -> String {
    let filtered_status_codes = status_codes
        .iter()
        .filter(|sc| sc.rfc.is_some() && sc.rfc_section.is_some());
    let constants: String = filtered_status_codes.clone().map(|sc| {
        format!(
            "
    /// {value} {reason_phrase}
    /// ([RFC{rfc}, Section {rfc_section}](https://tools.ietf.org/html/rfc{rfc}#section-{rfc_section}))
    pub const {const_name}: Self = Self::internal_from_const_u16({value});
",
            value = sc.value,
            reason_phrase = sc.reason_phrase,
            rfc = sc.rfc.unwrap(),
            rfc_section = sc.rfc_section.unwrap(),
            const_name = sc.reason_phrase.TO_SHOUTY_SNEK_CASE(),
        )
    }).collect();
    let constants_list: String = filtered_status_codes
        .map(|sc| {
            format!(
                "        Self::{},\n",
                sc.reason_phrase.TO_SHOUTY_SNEK_CASE()
            )
        })
        .collect();
    format!(
        "
{}

impl super::StatusCode {{
{}

    #[cfg(test)]
    pub(super) const GENERATED_CONSTANTS: &'static [Self] = &[
{}
    ];
}}\n",
        GENERATED_FILE_COMMENT, constants, constants_list,
    )
}
