use heck::ShoutySnakeCase;
use std::fs;

use crate::GENERATED_FILE_COMMENT;

const HEADER_NAMES_PATH: &str = "packages/code_gen/data/header_name_constants.json";
const GENERATED_CODE_PATH: &str = "packages/safe_http/src/header_name/constants.rs";

pub fn generate() {
    let json = fs::read_to_string(HEADER_NAMES_PATH).unwrap();
    let headers: Vec<Header> = serde_json::from_str(&json).unwrap();
    let generated_code =
        gen_header_name_constants(headers.iter().filter_map(|h| h.mdn.then(|| h.name)));
    fs::write(GENERATED_CODE_PATH, &generated_code).unwrap();
}

#[derive(serde::Deserialize)]
struct Header<'a> {
    name: &'a str,

    /// Has Mozilla Developer Network link
    mdn: bool,
}

fn gen_header_name_constants<'a>(
    header_names: impl IntoIterator<Item = &'a str> + Clone,
) -> String {
    let constants: String = header_names
        .clone()
        .into_iter()
        .map(|header_name| {
            format!(
                "
    /// The `{header_name}` header.
    ///
    /// [MDN | {header_name}](https://developer.mozilla.org/docs/Web/HTTP/Headers/{header_name})
    pub const {const_name}: Self = Self::from_static(\"{header_name}\");
",
                header_name = header_name,
                const_name = header_name.to_shouty_snake_case(),
            )
        })
        .collect();
    let constants_list: String = header_names
        .into_iter()
        .map(|header_name| format!("        Self::{},\n", header_name.to_shouty_snake_case()))
        .collect();
    format!(
        "\
{}

impl super::HeaderName {{
{}

    #[cfg(test)]
    pub(super) const GENERATED_CONSTANTS: &'static [Self] = &[
{}
    ];

}}",
        GENERATED_FILE_COMMENT, constants, constants_list
    )
}
