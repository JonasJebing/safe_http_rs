use std::fs;

use heck::CamelCase;

use crate::GENERATED_FILE_COMMENT;

const METHODS_PATH: &str = "packages/code_gen/data/methods.json";
const KIND_TEMPLATE: &str = "packages/code_gen/templates/method_kind.rs";
const KIND_TARGET: &str = "packages/safe_http/src/method/kind.rs";
const CONSTANTS_TARGET: &str = "packages/safe_http/src/method/constants.rs";

pub fn generate() {
    let json = fs::read_to_string(METHODS_PATH).unwrap();
    let methods: Vec<&str> = serde_json::from_str(&json).unwrap();
    let kind_code = gen_kind(&methods);
    fs::write(KIND_TARGET, &kind_code).unwrap();
    let constants_code = gen_constants(&methods);
    fs::write(CONSTANTS_TARGET, &constants_code).unwrap();
}

fn gen_kind(methods: &[&str]) -> String {
    let template = GENERATED_FILE_COMMENT.to_owned() + &fs::read_to_string(KIND_TEMPLATE).unwrap();
    let variants: String = methods
        .iter()
        .map(|m| format!("\n    {},", m.to_camel_case()))
        .collect();
    let standard_matches: String = methods
        .iter()
        .map(|m| {
            format!(
                "\n            \"{}\" => Some(Self::{}),",
                m,
                m.to_camel_case(),
            )
        })
        .collect();
    let as_str_matches: String = methods
        .iter()
        .map(|m| format!("\n            Self::{} => \"{}\",", m.to_camel_case(), m))
        .collect();
    template
        .replace("//{variants}", &variants)
        .replace("//{standard_matches}", &standard_matches)
        .replace("//{as_str_matches}", &as_str_matches)
}

fn gen_constants(methods: &[&str]) -> String {
    let constants: String = methods
        .iter()
        .map(|m| {
            format!(
                "
    /// The `{}` HTTP method.
    pub const {}: Self = Self(MethodKind::{});
",
                m,
                m,
                m.to_camel_case(),
            )
        })
        .collect();
    format!(
        "
{}
use super::MethodKind;

impl super::Method {{
{}
}}
    ",
        GENERATED_FILE_COMMENT, constants,
    )
}
