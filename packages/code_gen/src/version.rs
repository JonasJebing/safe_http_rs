use std::fs;

use crate::GENERATED_FILE_COMMENT;

const VERSIONS_PATH: &str = "packages/code_gen/data/versions.json";
const GENERATED_CODE_PATH: &str = "packages/safe_http/src/version/constants.rs";

pub fn generate() {
    let json = fs::read_to_string(VERSIONS_PATH).unwrap();
    let versions: Vec<&str> = serde_json::from_str(&json).unwrap();
    let generated_code = gen_version_constants(&versions);
    fs::write(GENERATED_CODE_PATH, &generated_code).unwrap();
}

fn gen_version_constants(versions: &[&str]) -> String {
    let constants: String = versions
        .iter()
        .map(|&version_str| {
            let version_tuple = version_str.split_once('.').unwrap();
            format!(
                "
    /// The `HTTP/{}` version.
    pub const HTTP_{}: Self = Self({}, {});
",
                version_str,
                version_str.replace('.', "_"),
                version_tuple.0,
                version_tuple.1,
            )
        })
        .collect();
    format!(
        "\
{}

impl super::Version {{
{}
}}",
        GENERATED_FILE_COMMENT, constants,
    )
}
