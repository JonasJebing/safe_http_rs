use crate::GENERATED_FILE_COMMENT;
use std::{fs, path::Path, rc::Rc};

type StaticStr = &'static str;
type RcString = Rc<String>;

const STRUCT_NAME_PLACEHOLDER: &str = "StructName";

pub fn generate() {
    let templates = Templates::read();
    let eq = |t: Templates| t.eq;
    let eq_ignore_case = |t: Templates| t.eq_ignore_case;
    let uri_params_array = [
        TemplateParams {
            struct_name: "Scheme",
            file_path: "packages/safe_uri/src/scheme/generated_struct.rs",
            default_str: Some("https"),
            attributes: "#[derive(Debug, Clone)]",
            additional_impls: eq_ignore_case,
        },
        TemplateParams {
            struct_name: "UserInfo",
            file_path: "packages/safe_uri/src/user_info/generated_struct.rs",
            default_str: None,
            attributes: "#[derive(Clone)]",
            additional_impls: eq_ignore_case,
        },
        TemplateParams {
            struct_name: "HostName",
            file_path: "packages/safe_uri/src/host_name/generated_struct.rs",
            default_str: None,
            attributes: "#[derive(Debug, Clone)]",
            additional_impls: eq_ignore_case,
        },
        TemplateParams {
            struct_name: "Path",
            file_path: "packages/safe_uri/src/path/generated_struct.rs",
            default_str: Some(""),
            attributes: "#[derive(Debug, Clone, PartialEq, Hash)]",
            additional_impls: eq,
        },
        TemplateParams {
            struct_name: "Query",
            file_path: "packages/safe_uri/src/query/generated_struct.rs",
            default_str: None,
            attributes: "#[derive(Debug, Clone, PartialEq, Hash)]",
            additional_impls: eq,
        },
        TemplateParams {
            struct_name: "Fragment",
            file_path: "packages/safe_uri/src/fragment/generated_struct.rs",
            default_str: None,
            attributes: "#[derive(Debug, Clone, PartialEq, Hash)]",
            additional_impls: eq,
        },
    ];
    let header_name_params = TemplateParams {
        struct_name: "HeaderName",
        file_path: "packages/safe_http/src/header_name/generated_struct.rs",
        default_str: None,
        attributes: "#[derive(Debug, Clone)]",
        additional_impls: eq_ignore_case,
    };
    for params in uri_params_array.into_iter().chain([header_name_params]) {
        params.generate_file(templates.clone());
    }
}

#[derive(Debug, Clone)]
struct Templates {
    main: RcString,
    default: RcString,
    eq_ignore_case: RcString,
    eq: RcString,
}

impl Templates {
    fn read() -> Self {
        let read_file_with_suffix = |suffix: &str| {
            let path = format!("packages/code_gen/templates/restricted_string{suffix}.rs");
            Rc::new(fs::read_to_string(&path).unwrap())
        };
        Self {
            main: read_file_with_suffix(""),
            default: read_file_with_suffix("_default"),
            eq_ignore_case: read_file_with_suffix("_eq_ignore_case"),
            eq: read_file_with_suffix("_eq"),
        }
    }
}

#[derive(Debug, Clone)]
struct TemplateParams {
    struct_name: StaticStr,
    file_path: StaticStr,
    default_str: Option<StaticStr>,
    attributes: StaticStr,
    additional_impls: fn(Templates) -> RcString,
}

impl TemplateParams {
    fn generate_file(&self, templates: Templates) {
        let code = self.generate_code(templates);
        let parent_dir = Path::new(self.file_path).parent().unwrap();
        fs::create_dir_all(parent_dir).unwrap();
        fs::write(self.file_path, &code).unwrap();
    }

    fn generate_code(&self, templates: Templates) -> String {
        let main_template = GENERATED_FILE_COMMENT.to_owned() + &templates.main;
        let default_impl = match self.default_str {
            Some(default_str) => templates.default.replace("//{default_str}", default_str),
            None => String::new(),
        };
        let additional_impls = (default_impl + &(self.additional_impls)(templates))
            .replace(STRUCT_NAME_PLACEHOLDER, self.struct_name);
        main_template
            .replace(STRUCT_NAME_PLACEHOLDER, self.struct_name)
            .replace("//{attributes}", self.attributes)
            .replace("//{additional_implementations}", &additional_impls)
    }
}
