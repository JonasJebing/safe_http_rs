use shared_bytes::SharedStr;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub(super) enum MethodKind {
    //{variants}
    Extension(SharedStr),
}

impl MethodKind {
    pub(super) fn standard(s: &str) -> Option<Self> {
        match s {
            //{standard_matches}
            _ => None,
        }
    }
}

impl MethodKind {
    pub(super) fn as_str(&self) -> &str {
        match self {
            //{as_str_matches}
            Self::Extension(s) => s,
        }
    }
}
