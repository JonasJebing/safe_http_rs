impl PartialEq<str> for StructName {
    fn eq(&self, other: &str) -> bool {
        self.as_str() == other
    }
}

impl PartialEq<String> for StructName {
    fn eq(&self, other: &String) -> bool {
        self.as_str() == other
    }
}
