impl StructName {
    #[inline]
    pub const fn new() -> Self {
        Self::from_static("//{default_str}")
    }
}

impl Default for StructName {
    #[inline]
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
#[test]
fn default_is_valid() {
    let default_str = StructName::default().into_shared_str();
    StructName::try_from(default_str).unwrap();
}
