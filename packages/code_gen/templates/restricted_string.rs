use super::{validate_static, validate_with_normalized_percent_encoding, InvalidStructName};
use shared_bytes::SharedStr;
use std::{hash::Hash, sync::Arc};

//{attributes}
pub struct StructName(SharedStr);

impl StructName {
    #[track_caller]
    #[inline]
    pub const fn from_static(string: &'static str) -> Self {
        match validate_static(string.as_bytes()) {
            Ok(()) => Self(SharedStr::from_static(string)),
            Err(_e) => panic!("invalid static StructName"),
        }
    }

    #[inline]
    pub fn as_shared_str(&self) -> &SharedStr {
        &self.0
    }

    #[inline]
    pub fn into_shared_str(self) -> SharedStr {
        self.0
    }

    #[inline]
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

//{additional_implementations}

impl AsRef<str> for StructName {
    #[inline]
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl Eq for StructName {}

impl From<StructName> for SharedStr {
    fn from(x: StructName) -> Self {
        x.into_shared_str()
    }
}

impl From<StructName> for String {
    fn from(x: StructName) -> Self {
        x.into_shared_str().into_string()
    }
}

impl<T> PartialEq<&'_ T> for StructName
where
    Self: PartialEq<T>,
    T: ?Sized,
{
    fn eq(&self, other: &&T) -> bool {
        self.eq(*other)
    }
}

impl TryFrom<&'static str> for StructName {
    type Error = InvalidStructName;

    fn try_from(value: &'static str) -> Result<Self, Self::Error> {
        let inner = validate_with_normalized_percent_encoding(value)?
            .unwrap_or_else(|| SharedStr::from(value));
        Ok(Self(inner))
    }
}

impl TryFrom<String> for StructName {
    type Error = InvalidStructName;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let inner = validate_with_normalized_percent_encoding(&value)?
            .unwrap_or_else(|| SharedStr::from(value));
        Ok(Self(inner))
    }
}

impl TryFrom<Arc<String>> for StructName {
    type Error = InvalidStructName;

    fn try_from(value: Arc<String>) -> Result<Self, Self::Error> {
        let inner = validate_with_normalized_percent_encoding(&value)?
            .unwrap_or_else(|| SharedStr::from(value));
        Ok(Self(inner))
    }
}

impl TryFrom<SharedStr> for StructName {
    type Error = InvalidStructName;

    fn try_from(value: SharedStr) -> Result<Self, Self::Error> {
        let inner = validate_with_normalized_percent_encoding(&value)?.unwrap_or(value);
        Ok(Self(inner))
    }
}
