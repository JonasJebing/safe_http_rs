use crate::{display::UriRefDisplay, Authority, ParseUriError, Resource, Scheme, Uri};
use shared_bytes::SharedStr;
use std::fmt;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Default)]
pub struct UriRef {
    pub scheme: Option<Scheme>,
    pub authority: Option<Authority>,
    pub resource: Resource,
    #[doc(hidden)]
    pub __private: (),
}

impl UriRef {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn parse(s: SharedStr) -> Result<Self, ParseUriError> {
        crate::parse::parse_uri_ref(s)
    }

    pub fn into_uri_with_default_scheme<F>(self, scheme: F) -> Uri
    where
        F: FnOnce() -> Scheme,
    {
        Uri {
            scheme: self.scheme.unwrap_or_else(scheme),
            authority: self.authority,
            resource: self.resource,
            __private: (),
        }
    }
}

impl From<Uri> for UriRef {
    fn from(uri: Uri) -> Self {
        Self {
            scheme: Some(uri.scheme),
            authority: uri.authority,
            resource: uri.resource,
            __private: (),
        }
    }
}

impl fmt::Display for UriRef {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", UriRefDisplay::from(self))
    }
}
