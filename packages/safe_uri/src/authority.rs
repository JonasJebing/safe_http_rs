use crate::{Host, UserInfo};

#[derive(Debug, Clone, PartialEq, Eq, Hash, Default)]
pub struct Authority {
    pub user_info: Option<UserInfo>,
    pub host: Host,
    pub port: Option<u16>,
    #[doc(hidden)]
    pub __private: (),
}

impl Authority {
    #[inline]
    pub fn new() -> Self {
        Self::default()
    }

    #[inline]
    pub fn from_host(host: Host) -> Self {
        Self {
            host,
            ..Self::new()
        }
    }
}

impl From<Host> for Authority {
    #[inline]
    fn from(host: Host) -> Self {
        Self::from_host(host)
    }
}
