use crate::{Fragment, Path, Query};

#[derive(Debug, Clone, PartialEq, Eq, Hash, Default)]
pub struct Resource {
    pub path: Path,
    pub query: Option<Query>,
    pub fragment: Option<Fragment>,
    #[doc(hidden)]
    pub __private: (),
}

impl Resource {
    #[inline]
    pub fn new() -> Self {
        Self::default()
    }

    #[inline]
    pub fn from_path(path: Path) -> Self {
        Self {
            path,
            ..Self::new()
        }
    }
}

impl From<Path> for Resource {
    #[inline]
    fn from(path: Path) -> Self {
        Self::from_path(path)
    }
}
