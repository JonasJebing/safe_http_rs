mod generated_struct;
mod generated_validate;

pub use self::generated_struct::HostName;

use crate::validation::{self, is_sub_delimiter, is_unreserved, InvalidComponent};
use shared_bytes::SharedStr;
use std::{error::Error, fmt};

const fn validate_static(bytes: &'static [u8]) -> Result<(), InvalidHostName> {
    match generated_validate::validate(bytes) {
        Ok(()) => Ok(()),
        Err(e) => Err(InvalidHostName(e)),
    }
}

const fn is_valid_byte(b: u8) -> bool {
    is_unreserved(b) || is_sub_delimiter(b)
}

fn validate_with_normalized_percent_encoding(
    string: &str,
) -> Result<Option<SharedStr>, InvalidHostName> {
    validation::with_normalized_percent_encoding(string, is_valid_byte).map_err(InvalidHostName)
}

#[derive(Debug, Clone)]
pub struct InvalidHostName(InvalidComponent);

impl fmt::Display for InvalidHostName {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "invalid host name: {}", self.0)
    }
}

impl Error for InvalidHostName {}
