mod generated_struct;

pub use self::generated_struct::Fragment;

use crate::validation::{self, query_or_fragment, InvalidComponent};
use shared_bytes::SharedStr;
use std::{error::Error, fmt};

const fn validate_static(bytes: &'static [u8]) -> Result<(), InvalidFragment> {
    match query_or_fragment::validate(bytes) {
        Ok(()) => Ok(()),
        Err(e) => Err(InvalidFragment(e)),
    }
}

fn validate_with_normalized_percent_encoding(
    string: &str,
) -> Result<Option<SharedStr>, InvalidFragment> {
    validation::with_normalized_percent_encoding(string, query_or_fragment::is_valid_byte)
        .map_err(InvalidFragment)
}

#[derive(Debug, Clone)]
pub struct InvalidFragment(InvalidComponent);

impl fmt::Display for InvalidFragment {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "invalid fragment: {}", self.0)
    }
}

impl Error for InvalidFragment {}
