mod generated_struct;
mod generated_validate;

pub use self::generated_struct::Path;

use crate::validation::{self, is_sub_delimiter, is_unreserved, InvalidComponent};
use shared_bytes::SharedStr;
use std::{error::Error, fmt};

const fn validate_static(bytes: &'static [u8]) -> Result<(), InvalidPath> {
    match generated_validate::validate(bytes) {
        Ok(()) => Ok(()),
        Err(e) => Err(InvalidPath(e)),
    }
}

fn validate_with_normalized_percent_encoding(
    string: &str,
) -> Result<Option<SharedStr>, InvalidPath> {
    validation::with_normalized_percent_encoding(string, is_valid_byte).map_err(InvalidPath)
}

const fn is_valid_byte(b: u8) -> bool {
    is_unreserved(b) || is_sub_delimiter(b) || matches!(b, b':' | b'@' | b'/')
}

#[derive(Debug, Clone)]
pub struct InvalidPath(InvalidComponent);

impl fmt::Display for InvalidPath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "invalid path: {}", self.0)
    }
}

impl Error for InvalidPath {}

#[cfg(test)]
mod tests {
    use super::*;
    use assert_matches::assert_matches;

    #[test]
    fn asterisk() {
        let string = "*";
        assert_matches!(Path::try_from(string), Ok(info) => assert_eq!(info, string));
    }
}
