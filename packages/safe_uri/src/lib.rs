/*!
# Examples

Constructing all parts of a Uri with and without
[`tap`](https://crates.io/crates/tap)
and [`Tap::tap_mut`](https://docs.rs/tap/1.0.1/tap/trait.Tap.html#method.tap_mut).

```
use safe_uri::*;
use tap::Tap;

let uri_with_tap = Uri::new().tap_mut(|u| {
    u.scheme = Scheme::try_from("https").unwrap();
    u.authority = Some(Authority::new().tap_mut(|a| {
        a.user_info = Some(UserInfo::try_from("alice").unwrap());
        a.host = Host::Name(HostName::try_from("example.com").unwrap());
        a.port = Some(443);
    }));
    u.resource = Resource::new().tap_mut(|r| {
        r.path = Path::try_from("/hello").unwrap();
        r.query = Some(Query::try_from("country=NL&city=Amsterdam").unwrap());
        r.fragment = Some(Fragment::try_from("greeting").unwrap());
    })
});

let mut authority = Authority::new();
authority.user_info = Some(UserInfo::try_from("alice").unwrap());
authority.host = Host::Name(HostName::try_from("example.com").unwrap());
authority.port = Some(443);
let mut resource = Resource::new();
resource.path = Path::try_from("/hello").unwrap();
resource.query = Some(Query::try_from("country=NL&city=Amsterdam").unwrap());
resource.fragment = Some(Fragment::try_from("greeting").unwrap());
let mut uri = Uri::new();
uri.scheme = Scheme::try_from("https").unwrap();
uri.authority = Some(authority);
uri.resource = resource;

assert_eq!(uri_with_tap, uri);
```
*/

#![forbid(unsafe_code)]

mod authority;
mod display;
mod fragment;
mod host;
mod host_name;
mod parse;
mod path;
mod percent_encoded;
mod query;
mod resource;
mod scheme;
mod uri;
mod uri_ref;
mod user_info;
mod validation;

pub use authority::Authority;
pub use fragment::{Fragment, InvalidFragment};
pub use host::Host;
pub use host_name::{HostName, InvalidHostName};
pub use parse::ParseUriError;
pub use path::{InvalidPath, Path};
pub use query::{InvalidQuery, Query};
pub use resource::Resource;
pub use scheme::{InvalidScheme, Scheme};
pub use uri::Uri;
pub use uri_ref::UriRef;
pub use user_info::{InvalidUserInfo, UserInfo};
