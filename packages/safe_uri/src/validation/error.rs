use crate::percent_encoded::InvalidPercentEncoded;
use std::{error::Error, fmt};

#[derive(Debug, Clone)]
pub(crate) enum InvalidComponent {
    Byte(InvalidByte),
    PercentEncoded(InvalidPercentEncoded),
    PercentEncodedByte(InvalidPercentEncodedByte),
}

// TODO: maybe remove this impl?
impl Error for InvalidComponent {}

impl fmt::Display for InvalidComponent {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            InvalidComponent::Byte(e) => e.fmt(f),
            InvalidComponent::PercentEncoded(e) => e.fmt(f),
            InvalidComponent::PercentEncodedByte(e) => e.fmt(f),
        }
    }
}

#[derive(Debug, Clone)]
pub(crate) struct InvalidByte {
    pub byte: u8,
}

impl fmt::Display for InvalidByte {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let byte = self.byte;
        let char = char::from(byte);
        write!(f, "byte {byte} ('{char}') is not valid")
    }
}

#[derive(Debug, Clone)]
pub(crate) struct InvalidPercentEncodedByte {
    pub(crate) byte: u8,
}

impl fmt::Display for InvalidPercentEncodedByte {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let byte = self.byte;
        let char = char::from(byte);
        write!(
            f,
            "valid byte {byte} ('{char}') shouldn't be percent encoded (%{byte:X})"
        )
    }
}
