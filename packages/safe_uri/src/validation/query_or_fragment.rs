mod generated_validate;

use crate::validation::{is_sub_delimiter, is_unreserved};

pub(crate) use self::generated_validate::validate;

pub(crate) const fn is_valid_byte(b: u8) -> bool {
    is_unreserved(b) || is_sub_delimiter(b) || matches!(b, b':' | b'@' | b'/' | b'?')
}
