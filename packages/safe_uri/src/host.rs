use crate::HostName;
use std::{
    fmt,
    net::{IpAddr, Ipv4Addr, Ipv6Addr},
};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
#[non_exhaustive] // allow future IP address versions or IPvFuture from RFC 3986
pub enum Host {
    Name(HostName),
    Ipv4Addr(Ipv4Addr),
    Ipv6Addr(Ipv6Addr),
}

impl Host {
    pub fn new() -> Self {
        Self::Ipv4Addr(Ipv4Addr::LOCALHOST)
    }
}

impl Default for Host {
    fn default() -> Self {
        Self::new()
    }
}

impl From<HostName> for Host {
    #[inline]
    fn from(x: HostName) -> Self {
        Self::Name(x)
    }
}

impl From<Ipv4Addr> for Host {
    #[inline]
    fn from(x: Ipv4Addr) -> Self {
        Self::Ipv4Addr(x)
    }
}

impl From<Ipv6Addr> for Host {
    #[inline]
    fn from(x: Ipv6Addr) -> Self {
        Self::Ipv6Addr(x)
    }
}

impl From<IpAddr> for Host {
    #[inline]
    fn from(i: IpAddr) -> Self {
        match i {
            IpAddr::V4(i) => Self::Ipv4Addr(i),
            IpAddr::V6(i) => Self::Ipv6Addr(i),
        }
    }
}

impl From<&Host> for Option<IpAddr> {
    #[inline]
    fn from(h: &Host) -> Self {
        match h {
            Host::Name(_) => None,
            Host::Ipv4Addr(i) => Some(IpAddr::V4(*i)),
            Host::Ipv6Addr(i) => Some(IpAddr::V6(*i)),
        }
    }
}

impl fmt::Display for Host {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Host::Name(x) => f.write_str(x.as_str()),
            Host::Ipv4Addr(x) => write!(f, "{}", x),
            Host::Ipv6Addr(x) => write!(f, "[{}]", x),
        }
    }
}
