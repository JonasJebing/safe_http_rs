mod generated_struct;

pub use self::generated_struct::Query;

use crate::validation::{self, query_or_fragment, InvalidComponent};
use shared_bytes::SharedStr;
use std::{error::Error, fmt};

const fn validate_static(bytes: &'static [u8]) -> Result<(), InvalidQuery> {
    match query_or_fragment::validate(bytes) {
        Ok(()) => Ok(()),
        Err(e) => Err(InvalidQuery(e)),
    }
}

fn validate_with_normalized_percent_encoding(
    string: &str,
) -> Result<Option<SharedStr>, InvalidQuery> {
    validation::with_normalized_percent_encoding(string, query_or_fragment::is_valid_byte)
        .map_err(InvalidQuery)
}

#[derive(Debug, Clone)]
pub struct InvalidQuery(InvalidComponent);

impl fmt::Display for InvalidQuery {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "invalid query: {}", self.0)
    }
}

impl Error for InvalidQuery {}
