use crate::{display::UriRefDisplay, Authority, ParseUriError, Resource, Scheme, UriRef};
use shared_bytes::SharedStr;
use std::fmt;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Default)]
pub struct Uri {
    pub scheme: Scheme,
    pub authority: Option<Authority>,
    pub resource: Resource,
    #[doc(hidden)]
    pub __private: (),
}

impl Uri {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn parse(s: SharedStr) -> Result<Self, ParseUriError> {
        crate::parse::parse_uri(s)
    }
}

impl TryFrom<UriRef> for Uri {
    type Error = UriRef;

    fn try_from(uri_ref: UriRef) -> Result<Self, Self::Error> {
        match uri_ref.scheme {
            Some(scheme) => Ok(Self {
                scheme,
                authority: uri_ref.authority,
                resource: uri_ref.resource,
                __private: (),
            }),
            None => Err(uri_ref),
        }
    }
}

impl fmt::Display for Uri {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", UriRefDisplay::from(self))
    }
}
