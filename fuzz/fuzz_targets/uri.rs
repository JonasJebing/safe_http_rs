#![no_main]
#![forbid(unsafe_code)]

use safe_uri::Uri;
use shared_bytes::SharedStr;
use std::sync::Arc;

type Input = Arc<String>;

libfuzzer_sys::fuzz_target!(|i: Input| {
    let _ = test(i);
});

fn test(input: Input) -> eyre::Result<()> {
    let input = SharedStr::from(input);
    let parsed = Uri::parse(input.clone())?;
    let parsed_as_string = SharedStr::from(parsed.to_string());
    let re_parsed = Uri::parse(parsed_as_string.clone()).unwrap();
    assert_eq!(parsed, re_parsed);
    assert_eq!(parsed_as_string, re_parsed.to_string());
    let may_have_port_with_leading_zeroes = has_port(&parsed) && input.contains('0');
    if !may_have_port_with_leading_zeroes && !input.contains('%') {
        assert_eq!(input, parsed.to_string());
    }
    Ok(())
}

fn has_port(uri: &Uri) -> bool {
    uri.authority.as_ref().and_then(|a| a.port).is_some()
}
