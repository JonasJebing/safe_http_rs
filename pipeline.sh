#!/usr/bin/env bash


echo "---fmt"
cargo fmt || exit
echo
echo "---build"
cargo build --all-features || exit
echo
echo "---test"
cargo test || exit
echo
echo "---test --all-features"
cargo test --all-features || exit
echo
echo "---clippy --all-targets"
cargo clippy --all-targets || exit
echo
echo "---clippy --all-targets --all-features"
cargo clippy --all-targets --all-features || exit
echo
echo "---doc"
cargo doc || exit
